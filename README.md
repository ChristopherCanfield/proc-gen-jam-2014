#Procedural Generation Jam 2014
------------------------------------------

Repository for the game creator for the Procjam 2014, the Procedural Generation Jam.

http://procjam.tumblr.com/  
http://itch.io/jam/procjam/

##What is PROCJAM?

PROCJAM is an online jam for making games and other things inspired by procedural generation. It’s launching with a day of talks, streamed online from a live London event at Goldsmiths University!

##From the description:

There are two ways to enter:

1. Make a game with procedural generation in it. Maybe a Twine adventure with randomised character personalities? Maybe an action-RPG where each player gets their own procedural theme tune? Maybe an old-fashioned world-generator for a strategy game? Create a game for #procjam using the optional theme (announced at the start of the jam) and include a procedural twist in there somewhere.
2. Make a tool that generates stuff to help game developers. We already have amazing tools like sound effect generator SFXR, music generation like Abundant Music, or random sprite-grabbers like Spritely. What other tools could we make to help people generate cool things for the games they make? Maybe a corporation generator for cyberpunk cities? A tool for generating alien alphabet fonts? A library that automatically generates enemy ships for space shooters?