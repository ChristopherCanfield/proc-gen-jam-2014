package com.divergentthoughtsgames.ants.android;

import android.os.Bundle;
import android.view.WindowManager;

import com.badlogic.gdx.backends.android.AndroidApplication;
import com.badlogic.gdx.backends.android.AndroidApplicationConfiguration;
import com.divergentthoughtsgames.ants.ExtendedGame;

public class AndroidLauncher extends AndroidApplication {
	@Override
	protected void onCreate (Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		// From https://developer.android.com/training/scheduling/wakelock.html
		getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
		
		AndroidApplicationConfiguration config = new AndroidApplicationConfiguration();
		initialize(new ExtendedGame(), config);
	}
}
