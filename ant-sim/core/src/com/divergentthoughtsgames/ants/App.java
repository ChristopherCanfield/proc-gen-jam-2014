package com.divergentthoughtsgames.ants;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputMultiplexer;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.input.GestureDetector;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.utils.Timer;
import com.divergentthoughtsgames.ants.audio.AudioSystem;
import com.divergentthoughtsgames.ants.graphics.GraphicsSystem;
import com.divergentthoughtsgames.ants.input.KeyboardCameraController;
import com.divergentthoughtsgames.ants.input.MouseCameraController;
import com.divergentthoughtsgames.ants.input.SystemKeyProcessor;
import com.divergentthoughtsgames.ants.input.TouchCameraController;
import com.divergentthoughtsgames.ants.world.World;

public abstract class App
{
	public static final String CompanyName = "divergentthoughtsgames";
	public static final String GameName = "ants";
	
	/**
	 * Paths to resources.
	 */
	public static final AppPath paths = new AppPath();
	
	/**
	 * The manages the application states.
	 */
	public static ExtendedGame state;
	
	/**
	 * Sets the game state.
	 * @param state the new game state.
	 */
	public static void setState(ExtendedGame state)
	{
		App.state = state;
	}
	
	// The total elapse game time.
	private static double gameTime;
	
	/**
	 * Returns the total elapsed game time.
	 * @return the total elapsed game time.
	 */
	public static double getGameTime()
	{
		return gameTime;
	}
	
	/**
	 * Debugging settings. Use constants in the Debug class.
	 */
	public static int debugSettings = Debug.Disabled;
	
	/**
	 * Initializes the game: sets the graphics system and input processors, loads the user's
	 * preferences, and instantiates the ExecutorService.
	 */
	public static void initialize()
	{
		App.graphics = new GraphicsSystem();
		
		setInputProcessors(graphics);
		
		Settings settings = getSettings();
		if (settings.load())
		{
			audio.setAmbientVolume(settings.getInteger(Settings.AmbientSoundsVolume));
			audio.setMusicVolume(settings.getInteger(Settings.MusicVolume));
			audio.setSoundEffectVolume(settings.getInteger(Settings.SoundEffectsVolume));
		
			autosaver = new AutoSaver(settings.getInteger(Settings.AutosaveFrequency));
		}
		else
		{
			audio.setAmbientVolume(75);
			audio.setMusicVolume(50);
			audio.setSoundEffectVolume(100);
			
			autosaver = new AutoSaver(3);
		}
		
		taskQueue = Executors.newFixedThreadPool(2);
		autosaver.start();
	}
	
	/**
	 * Sets the game world.
	 * @param world the world to set.
	 */
	public static void setWorld(World world)
	{
		App.world = world;
	}
	
	private static void setInputProcessors(GraphicsSystem graphics)
	{
		InputMultiplexer inputProcessors = new InputMultiplexer();
//		inputProcessors.addProcessor(new ClickLocator());
//		inputProcessors.addProcessor(new SelectionInputProcessor(graphics));
//		inputProcessors.addProcessor(new UnitGroupKeyProcessor());
//		inputProcessors.addProcessor(new UnitControlInputProcessor());
		inputProcessors.addProcessor(new MouseCameraController(graphics.getCamera()));
//		inputProcessors.addProcessor(new DebugKeyProcessor());
		inputProcessors.addProcessor(new GestureDetector(new TouchCameraController(graphics.getCamera())));
		inputProcessors.addProcessor(new SystemKeyProcessor());
		Gdx.input.setInputProcessor(inputProcessors);
		
		graphics.addCameraController(new KeyboardCameraController(graphics.getCamera()));
	}
	
	/**
	 * Reference to the graphics system.
	 */
	public static GraphicsSystem graphics;
	
	/**
	 * Reference to the audio system.
	 */
	public static final AudioSystem audio = new AudioSystem();
	
	/**
	 * Reference to the game world.
	 */
	public static World world;
	
	/**
	 * The game's AssetManager, which manages heavy-weight resources.
	 */
	public static final AssetManager assetManager = new AssetManager();
	
	/**
	 * Enables tasks to be scheduled to be run on the main thread.
	 */
	public static final Timer timer = Timer.instance();
	
	/**
	 * An executor service for short running tasks off of the main thread.
	 */
	public static ExecutorService taskQueue;
	
	/**
	 * Shortcut for the App.graphics.getCamera().unproject method. The x- and y-coordinates are 
	 * assumed to be in screen coordinates (origin is the top left corner, y pointing down, x 
	 * pointing to the right) as reported by the touch methods in Input.
	 * @param x the x coordinate, in screen coordinates.
	 * @param y the y coordinate, in screen coordinates.
	 * @return Vector3 containing the converted world coordinates.
	 */
	public static Vector3 screenToWorld(int x, int y)
	{
		return App.graphics.getCamera().unproject(new Vector3(x, y, 0));
	}
	
	/**
	 * Shortcut for the App.graphics.getCamera().project method. The viewport is assumed to span 
	 * the whole screen. The screen coordinate system has its origin in the bottom left, with the 
	 * y-axis pointing upwards and the x-axis pointing to the right.
	 * @param x the x coordinate, in world coordinates.
	 * @param y the y coordinate, in world coordinates.
	 * @return Vector3 containing the converted world coordinates.
	 */
	public static Vector3 worldToScreen(int x, int y)
	{
		return App.graphics.getCamera().project(new Vector3(x, y, 0));
	}
	
	/**
	 * The number of pixels per tile.
	 */
	public static final float PixelsPerTile = 64f;
	
	/**
	 * The number of tiles per pixel.
	 */
	public static final float TilePerPixel = 1 / PixelsPerTile;
	
	/**
	 * Updates the game time. Must be called once per tick. 
	 */
	public static void update()
	{
		gameTime += Gdx.graphics.getRawDeltaTime();
	}
	
	private static AutoSaver autosaver;
	
	// Whether the game is paused or not.
	private static boolean paused = false;
	
	/**
	 * Specifies whether the game is paused.
	 * @return true if the game is paused, or false otherwise.
	 */
	public static boolean isPaused() 
	{ 
		return paused; 
	}
	
	/**
	 * Sets whether the game is paused or not. Also pauses or unpauses the audio system.
	 * @param paused true if the game should be paused, or false to unpause.
	 */
	public static void setPaused(boolean paused) 
	{ 
		App.paused = paused;
		
		if (paused)
		{
			App.audio.pause();
			App.autosaver.stop();
		}
		else
		{
			App.audio.unpause();
			App.autosaver.start();
		}
	}
	
	// The application's settings instance.
	private static Settings settings = new Settings();
	
	/**
	 * Returns the Preferences instance, which can be used for saving and loading game settings.
	 * @return the game's Preferences instance.
	 */
	public static Settings getSettings()
	{
		return settings;
	}
	
	/**
	 * The name of the game map that is loaded.
	 */
	public static String mapName;
	
	/**
	 * Shuts down all subsystems, and exits the application.
	 */
	public static void exit()
	{
		Gdx.app.debug("App", "Exit called");
		
		timer.stop();
		if (taskQueue != null) taskQueue.shutdown();
		if (state != null) state.dispose();

		Gdx.app.exit();
	}
}
