package com.divergentthoughtsgames.ants;

import org.apache.commons.lang3.SystemUtils;

public class AppPath
{
	public final String Ui = "ui/";
	public final String Textures = "textures/";
	public final String Music = "music/";
	public final String Sfx = "sfx/";
	public final String Ambient = "ambient/";
	public final String Maps = "maps/";
	
	/**
	 * Gets the path for the application's user data. This is not usable or relevant to mobile devices.
	 * @return the path for the application's user data.
	 */
	public static String getUserDataPath()
	{
		final String separator = SystemUtils.FILE_SEPARATOR;
		final String appDataPath = App.CompanyName + separator + App.GameName + separator; 
		
		if (SystemUtils.IS_OS_WINDOWS)
		{
			return System.getenv("APPDATA") + separator + appDataPath;
		}
		else if (SystemUtils.IS_OS_MAC_OSX)
		{
			return SystemUtils.USER_HOME + separator + "Library" + separator + appDataPath; 
		}
		else
		{
			return "." + appDataPath;
		}
	}
	
	/**
	 * Gets the application's save path. This is not usable or relevant to mobile devices.
	 * @return the application's save path.
	 */
	public static String getSavePath()
	{
		final String separator = SystemUtils.FILE_SEPARATOR;
		return getUserDataPath() + "saves" + separator;
	}
	
	/**
	 * Gets the application's screenshot save path. This is not usable or relevant to mobile devices.
	 * @return the application's screenshot save path.
	 */
	public static String getScreenshotPath()
	{
		final String separator = SystemUtils.FILE_SEPARATOR;
		return getUserDataPath() + "screenshots" + separator;
	}
	
	/**
	 * Gets the application's preferences path, without a file name. This is not usable or relevant 
	 * to mobile devices.
	 * @return the application's preferences path.
	 */
	public static String getPreferencesPath()
	{
		final String separator = SystemUtils.FILE_SEPARATOR;
		if (SystemUtils.IS_OS_MAC_OSX)
		{
			return SystemUtils.USER_HOME + separator + "Library" + separator + "Preferences" + separator + App.CompanyName + separator + App.GameName + separator;
		}
		else
		{
			return getUserDataPath();
		}
	}
}
