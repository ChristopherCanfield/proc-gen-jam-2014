package com.divergentthoughtsgames.ants;

import com.badlogic.gdx.utils.Timer;

/**
 * Automatically saves the game periodically.
 * @author Christopher D. Canfield
 */
public class AutoSaver extends Timer.Task
{
	private float autosaveFrequency;
	
	private int nextAutosaveIndex;
	private int maxAutosaveIndex = 3;
	
	private boolean isScheduled;
	
	/**
	 * Constructs an AutoSaver instance.
	 * @param saveFrequencyMinutes the number of minutes between saves.
	 */
	public AutoSaver(int saveFrequencyMinutes)
	{
		if (saveFrequencyMinutes <= 0)
		{
			throw new IllegalArgumentException("Invalid save frequency for AutoSaver: " + saveFrequencyMinutes);
		}
		
		autosaveFrequency = saveFrequencyMinutes * 60;
	}
	
	/**
	 * Sets the autosave frequency, in minutes.
	 * @param minutes the number of minutes in between autosaves.
	 */
	public void setAutosaveFrequency(int minutes)
	{
		autosaveFrequency = minutes * 60;
	}
	
	/**
	 * Starts the autosaver. It will continue running until stop is called.
	 */
	public void start()
	{
		if (!isScheduled)
		{
			isScheduled = true;
			App.timer.scheduleTask(this, autosaveFrequency);
		}
	}
	
	/**
	 * Stops the autosaver.
	 */
	public void stop()
	{
		cancel();
		isScheduled = false;
	}

	@Override
	public void run()
	{
		final String saveName = "autosave_" + nextAutosaveIndex;
		// Set the next autosave index. Loop back to zero if the current index equals or exceeds the 
		// max index.
		nextAutosaveIndex = (nextAutosaveIndex >= maxAutosaveIndex) ? 0 : nextAutosaveIndex + 1;
		
		SavedGame savedGame = new SavedGame();
		savedGame.serialize(saveName);
		
		App.timer.scheduleTask(this, autosaveFrequency);
	}
}
