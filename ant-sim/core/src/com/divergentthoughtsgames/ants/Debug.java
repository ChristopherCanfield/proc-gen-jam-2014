package com.divergentthoughtsgames.ants;

/**
 * Debugging constants.
 * @author Christopher D. Canfield
 */
public abstract class Debug
{
	/** Disable all debugging. **/
	public static final int Disabled = 0x0000;
	
	/** Draw user interface outlines. **/ 
	public static final int DrawUiOutlines = 0x0001;
	
	/** Draw the navigation nodes. **/
	public static final int DrawNavNodes = 0x0002;
	
	/** Draw the navigation path edges. **/
	public static final int DrawNavPaths = 0x0004;
	
	public static boolean isEnabled(int debugConstant)
	{
		return (App.debugSettings & debugConstant) != 0;
	}
}
