package com.divergentthoughtsgames.ants;

/**
 * The entry point to the application.
 * @author Christopher D. Canfield
 */
public class ExtendedGame extends com.badlogic.gdx.Game 
{
	@Override
	public void create() 
	{
		App.setState(this);
		App.state.setScreen(new MenuTestState());
	}

	@Override
	public void render() 
	{
		App.update();
		super.render();
	}
}
