package com.divergentthoughtsgames.ants;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

import org.ini4j.Ini;
import org.ini4j.InvalidFileFormatException;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.maps.tiled.TiledMapTile.BlendMode;
import com.badlogic.gdx.maps.tiled.TiledMapTileLayer;
import com.badlogic.gdx.maps.tiled.TiledMapTileLayer.Cell;
import com.divergentthoughtsgames.ants.nav.Edge;
import com.divergentthoughtsgames.ants.nav.NavMap;
import com.divergentthoughtsgames.ants.nav.Node;
import com.divergentthoughtsgames.ants.world.World;

/**
 * Creates the game world from a TMX game map (Tiled) as well as information specifying which resources
 * are used in the map. 
 * @author Christopher D. Canfield
 */
public class MapLoader
{
	/**
	 * Processes the tmx map from the asset manager, loads the ini file associated with the map, 
	 * creates the navigation graph, and sets the music and ambient sounds playlists.
	 * @param mapName the name of the map file, without the path or extension.
	 * @return the game world.
	 */
	public World load(String mapName)
	{
		TiledMap map = App.assetManager.get(App.paths.Maps + mapName + ".tmx", TiledMap.class);
		App.graphics.setMap(map);
		App.mapName = mapName;
		
		disableBlending(map);
		
		World world = createWorld(map);
		
		try {
			loadIni(mapName);
		} catch(Exception e) {
			Gdx.app.error("Map Loading", "Failed loading ini: " + e.getMessage());
			// TODO (10/28/2014): Load default playlists when loading from the ini fails.
		}
		
		return world;
	}

	/**
	 * Disables alpha blending in all tiles, since it's not needed. According to https://github.com/libgdx/libgdx/wiki/Tile-maps,
	 * this improves performance, but this should be checked with real data.
	 * @param map the Tiled map.
	 */
	private static void disableBlending(TiledMap map)
	{
		TiledMapTileLayer layer = (TiledMapTileLayer)map.getLayers().get(0);
		final int rowCount = layer.getHeight();
		final int columnCount = layer.getWidth();
		
		for (int row = 0; row < rowCount; row++)
		{
			for (int column = 0; column < columnCount; column++)
			{
				Cell cell = layer.getCell(column, row);
				cell.getTile().setBlendMode(BlendMode.NONE);
			}
		}
	}
	
	/**
	 * Loads the data from the map ini file, and sets the music and ambient sound playlists. 
	 * @param mapName the name of the map file, without the path or extension.
	 * @throws IOException if there is an I/O problem with opening and reading the ini file.
	 * @throws InvalidFileFormatException if the map ini file is not formatted properly.
	 */
	private static void loadIni(String mapName) throws InvalidFileFormatException, IOException
	{
		FileHandle mapIniFile = Gdx.files.internal(App.paths.Maps + mapName + ".ini");
		
		Ini mapIni = new Ini();
		mapIni.load(mapIniFile.reader());
		
		// Load the music playlist.
		Ini.Section musicSection = mapIni.get("music");
		List<String> musicPlaylist = musicSection.getAll("playlist");
		for (int i = 0; i < musicPlaylist.size(); i++)
		{
			musicPlaylist.set(i, App.paths.Music + musicPlaylist.get(i));
		}
		App.audio.setMusicPlaylist(musicPlaylist);
		
		// Load the ambient sounds playlist.
		Ini.Section ambientSection = mapIni.get("ambient");
		List<String> ambientPlaylist = ambientSection.getAll("playlist");
		for (int i = 0; i < ambientPlaylist.size(); i++)
		{
			ambientPlaylist.set(i, App.paths.Ambient + ambientPlaylist.get(i));
		}
		App.audio.setAmbientPlaylist(ambientPlaylist);
	}
	
	/**
	 * Creates the navigation graph from the Tiled map data.
	 * @param map the Tiled map.
	 */
	private static World createWorld(TiledMap map)
	{
		int columns = ((TiledMapTileLayer)map.getLayers().get(0)).getWidth();
		int rows = ((TiledMapTileLayer)map.getLayers().get(0)).getHeight();
		
		// The navigation list.
		ArrayList<Node> nodeList = new ArrayList<Node>();
		// The navigation graph, in [column][row] layout.
		Node[][] nodes = new Node[columns][rows];
		
		createNavGraph(nodeList, nodes, columns, rows);
		setNodePassability(map, nodes[0][0]);
		
		// Create world, and add the navigation graph to it.
		World world = new World(columns, rows);
		Node[] navList = nodeList.toArray(new Node[nodeList.size()]);
		NavMap navMap = new NavMap(navList, nodes);
		world.setNavMap(navMap);
		
		return world;
	}
	
	/**
	 * Creates the structure of the navigation graph.
	 * @param nodeList an instantiated list that will be populated with the navigation nodes.
	 * @param nodes a 2-dimensional array that will be populated with the navigation nodes, in 
	 * [column][row] notation. The array bounds must be equal to the number of columns and rows in 
	 * the map. 
	 * @param columns the number of columns in the map.
	 * @param rows the number of rows in the map.
	 */
	private static void createNavGraph(List<Node> nodeList, Node[][] nodes, int columns, int rows)
	{
		// Build the navigation graph. 0,0 = bottom left.
		// For now, the navigation graph has one node per map cell, but this may change in the future.
		for (int row = 0; row < rows; ++row)
		{
			for (int column = 0; column < columns; ++column)
			{
				Node node = new Node(column * Node.Size, row * Node.Size);
				nodeList.add(node);
				nodes[column][row] = node;
				addEdges(nodes, row, column, columns);
			}
		}
	}
	
	/**
	 * Adds edges to the specified nodes.
	 * @param nodes the navigation graph, in [column][row] order.
	 * @param row the node's row.
	 * @param column the node's column.
	 * @param columns the number of columns in the navigation graph.
	 */
	private static void addEdges(Node[][] nodes, int row, int column, int columns)
	{
		Node node = nodes[column][row];
		
		// Add down.
		if (row > 0)
		{
			Edge edge = new Edge(1);
			edge.addNode(node).addNode(nodes[column][row - 1]);
		}
		
		// Add left.
		if (column > 0)
		{
			Edge edge = new Edge(1);
			edge.addNode(node).addNode(nodes[column - 1][row]);
		}
		
		// Add lower-left diagonal.
		if (row > 0 && column > 0)
		{
			Edge edge = new Edge(1.4);
			edge.addNode(node).addNode(nodes[column - 1][row - 1]);
		}
		
		// Add lower-right diagonal.
		if (row > 0 && column < columns - 1)
		{
			if (nodes[column + 1][row - 1] != null)
			{
				Edge edge = new Edge(1.4);
				edge.addNode(node).addNode(nodes[column + 1][row - 1]);
			}
		}
	}
	
	/**
	 * Sets navigation graph passability based on the Tiled map "type" property for each cell.
	 * Nodes that are normally passable, but that are unreachable from the main navigation graph,
	 * are also flagged as impassable using an adapted breadth first search algorithm.
	 * @param map the Tiled map.
	 * @param startNode the first node to check; This must be part of the main navigation graph.
	 */
	private static void setNodePassability(TiledMap map, Node startNode)
	{
		startNode.setPassable(true);
		TiledMapTileLayer layer = (TiledMapTileLayer)map.getLayers().get(0);
		
		Queue<Node> unexplored = new LinkedList<Node>();
		unexplored.add(startNode);
		
		HashSet<Node> searched = new HashSet<Node>();
		searched.add(startNode);
		
		while (!unexplored.isEmpty())
		{
			Node current = unexplored.remove();
			for (Edge e : current.getEdges())
			{
				Node adjacent = e.getOppositeNode(current);
				if (!searched.contains(adjacent))
				{
					searched.add(adjacent);
					
					int column = adjacent.getColumnIndex();
					int row = adjacent.getRowIndex();
					String terrainType = (String)layer.getCell(column, row).getTile().getProperties().get("type");
					
					if (!terrainType.equals("water"))
					{
						Gdx.app.debug("Map Loading", "Cell (" + column + ", " + row + ") is passable");
						unexplored.add(adjacent);
						adjacent.setPassable(true);
					}
				}
			}
		}
	}
}
