package com.divergentthoughtsgames.ants;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.ScreenAdapter;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Table;

public abstract class MenuAppState extends ScreenAdapter
{
	/** The scene2d.ui stage. **/
	protected final Stage stage;
	
	/** The base table for the screen. **/
	protected final Table table;

	public MenuAppState()
	{
		this.stage = new Stage();
		this.table = new Table();
		
		table.setFillParent(true);
		table.top();
		stage.addActor(table);
		
		Gdx.input.setInputProcessor(stage);
	}

	@Override
	public final void render(float delta)
	{
		Gdx.gl.glClearColor(0, 0, 0, 1);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

		stage.act();
		onUpdate();
		
		boolean debug = (App.debugSettings & Debug.DrawUiOutlines) != 0;
		stage.setDebugAll(debug);
		
		stage.draw();
	}
	
	/**
	* Called once per tick. Child classes can override this if necessary.
	*/
	protected void onUpdate()
	{
	}
	
	/**
	* Releases all heavy-weight resources.
	*/
	@Override
	public abstract void dispose();
}
