package com.divergentthoughtsgames.ants;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.io.Serializable;
import java.util.zip.DeflaterOutputStream;
import java.util.zip.InflaterInputStream;

import org.apache.commons.io.IOUtils;

import com.badlogic.gdx.Application.ApplicationType;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.math.Vector3;
import com.divergentthoughtsgames.ants.nav.NavMap;
import com.divergentthoughtsgames.ants.world.World;

/**
 * Serializes and deserializes the game state.
 * @author Christopher D. Canfield
 */
public class SavedGame implements Serializable
{
	private static final long serialVersionUID = 1L;
	
	private World world;
	
	private Vector3 position;
	private float zoom;
	
	private String mapName;
	
	/**
	 * Serializes the game state to disk.
	 * @param saveName the name of the saved game, excluding the file extension.
	 */
	public void serialize(String saveName)
	{
		getValues();
		
		ObjectOutputStream out = null;
		try
		{
			// TODO (10/30/2014): Handle exceptions.
			OutputStream saveFile = null;
			if (Gdx.app.getType() == ApplicationType.Desktop)
			{
				String path = AppPath.getSavePath();
				(new File(path)).mkdirs();
				saveFile = new BufferedOutputStream(new FileOutputStream(path + saveName + ".sav"));
			}
			else
			{
				saveFile = Gdx.files.local(saveName + ".sav").write(false);
			}
			
			out = new ObjectOutputStream(new DeflaterOutputStream(saveFile));
			out.writeObject(this);
			
			Gdx.app.debug("Saved Game", "Game saved: " + saveName + ".sav");
		}
		catch(IOException e)
		{
			// TODO (10/30/2014): Handle exceptions.
			e.printStackTrace();
		}
		finally
		{
			IOUtils.closeQuietly(out);
		}
	}
	
	/**
	 * Deserializes the game state, and loads it into the application.
	 * @param saveName the name of the saved game, excluding the file extension.
	 */
	public static void deserialize(String saveName)
	{
		// TODO (10/30/2014): Ensure that the file exists.
		// TODO (11/1/2014): If loading fails, the current application state should be retained.
		
		ObjectInputStream in = null;
		try
		{
			App.initialize();
			
			InputStream saveFile = null;
			if (Gdx.app.getType() == ApplicationType.Desktop)
			{
				String path = AppPath.getSavePath();
				saveFile = new BufferedInputStream(new FileInputStream(path + saveName + ".sav"));
			}
			else
			{
				saveFile = Gdx.files.local(saveName + ".sav").read();
			}
			
			in = new ObjectInputStream(new InflaterInputStream(saveFile));
			SavedGame savedGame = (SavedGame)in.readObject();
			
			setValues(savedGame);
			
			Gdx.app.debug("Saved Game", "Game loaded: " + saveName + ".sav");
		}
		catch (ClassNotFoundException | IOException e)
		{
			// TODO (10/30/2014): Handle exceptions.
			e.printStackTrace();
		}
		finally
		{
			IOUtils.closeQuietly(in);
		}
	}
	
	/**
	 * Loads the values that will be serialized from the application. 
	 */
	private void getValues()
	{
		world = App.world;
		mapName = App.mapName;
		
		OrthographicCamera camera = App.graphics.getCamera();
		position = camera.position;
		zoom = camera.zoom;
	}
	
	/**
	 * Puts the values that were loaded from the save file into the application.
	 * @param savedGame the SavedGame instance.
	 */
	private static void setValues(SavedGame savedGame)
	{
		MapLoader mapLoader = new MapLoader();
		mapLoader.load(savedGame.mapName);
		
		NavMap navMap = App.world.getNavMap();

		App.world = savedGame.world;
		App.world.setNavMap(navMap);
		
		OrthographicCamera camera = App.graphics.getCamera();
		camera.position.set(savedGame.position);
		camera.zoom = savedGame.zoom;
	}
}
