package com.divergentthoughtsgames.ants;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Properties;

import org.apache.commons.io.IOUtils;

import com.badlogic.gdx.Application.ApplicationType;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.Preferences;

/**
 * Contains the application's settings. Has methods to persist and load the settings from disk.
 * @author Christopher D. Canfield
 */
public class Settings
{
	public static final String SoundEffectsVolume = "SoundEffectsVolume";
	public static final String MusicVolume = "MusicVolume";
	public static final String AmbientSoundsVolume = "AmbientSoundsVolume";
	
	public static final String AutosaveFrequency = "AutosaveFrequency";
	
	public static final String KeyQuickSave = "KeyQuickSave";
	public static final String KeyQuickLoad = "KeyQuickLoad";
//	public static final String KeyScreenshot = "KeyScreenshot";
	
	private static final String fileName = "settings.pref";
	
	private Properties settings;
	private Properties defaultSettings ;
	
	/**
	 * Constructs a Settings instance.
	 */
	public Settings()
	{
		defaultSettings = new Properties();
		
		// Volume
		defaultSettings.setProperty(SoundEffectsVolume, Integer.toString(100));
		defaultSettings.setProperty(MusicVolume, Integer.toString(50));
		defaultSettings.setProperty(AmbientSoundsVolume, Integer.toString(75));
		
		// Misc
		defaultSettings.setProperty(AutosaveFrequency, Integer.toString(3));
		
		// Keys
		defaultSettings.setProperty(KeyQuickSave, Integer.toString(Keys.F6));
		defaultSettings.setProperty(KeyQuickLoad, Integer.toString(Keys.F9));
		
		settings = new Properties(defaultSettings);
		settings.putAll(defaultSettings);
	}
	
	/**
	 * Loads the settings from disk.
	 * @return true if the settings were loaded successfully, or false otherwise.
	 */
	public boolean load()
	{
		if (Gdx.app.getType() == ApplicationType.Desktop)
		{
			return loadFromDesktop();
		}
		else
		{
			// No need to pre-load from mobile.
			return true;
		}
	}
	
	/**
	 * Saves the settings to disk.
	 * @return true if the settings were saved successfully, or false otherwise.
	 */
	public boolean save()
	{
		if (Gdx.app.getType() == ApplicationType.Desktop)
		{
			return saveToDesktop();
		}
		else
		{
			return saveToMobile();
		}
	}
	
	/**
	 * Loads the settings from a desktop operating system.
	 * @return true if the settings were loaded successfully, or false otherwise.
	 */
	private boolean loadFromDesktop()
	{
		InputStream settingsStream = null;
		String path = AppPath.getPreferencesPath();
		
		try
		{
			File settingsFile = new File(path + fileName);
			if (settingsFile.exists() && settingsFile.isFile())
			{
				settingsStream = new BufferedInputStream(new FileInputStream(path + fileName));
				settings.load(settingsStream);
			}
			else
			{
				saveToDesktop();
			}
			
			return true;
		}
		catch (IOException e)
		{
			Gdx.app.debug("Settings", "Error opening settings file: " + e.getMessage());
			return false;
		}
		finally
		{
			IOUtils.closeQuietly(settingsStream);
		}
	}
	
	/**
	 * Saves the settings on a desktop operating system.
	 * @return true if the settings were saved successfully, or false otherwise.
	 */
	private boolean saveToDesktop()
	{
		OutputStream propertiesFile = null;
		try
		{
			String path = AppPath.getPreferencesPath();
			(new File(path)).mkdirs();
			propertiesFile = new BufferedOutputStream(new FileOutputStream(path + fileName));
			settings.store(propertiesFile, null);
			return true;
		}
		catch (IOException e)
		{
			Gdx.app.error("Settings", "Error saving settings file: " + e.getMessage());
			return false;
		}
		finally
		{
			IOUtils.closeQuietly(propertiesFile);
		}
	}
	
	/**
	 * Saves the settings on a mobile device.
	 * @return true if the settings were saved successfully, or false otherwise.
	 */
	private static boolean saveToMobile()
	{
		try
		{
			Gdx.app.getPreferences(fileName).flush();
			return true;
		}
		catch (Exception e)
		{
			Gdx.app.error("Settings", "Error saving settings file: " + e.getMessage());
			return false;
		}
	}
	
	/**
	 * Returns the specified setting as an integer.
	 * @param settingName the name of the setting to get.
	 * @return the specified setting as an integer.
	 */
	public int getInteger(String settingName)
	{
		int propertyValue = Integer.valueOf(settings.getProperty(settingName));
		if (Gdx.app.getType() == ApplicationType.Desktop)
		{
			return propertyValue; 
		}
		else
		{
			return Gdx.app.getPreferences(fileName).getInteger(settingName, propertyValue);
		}
	}
	
	/**
	 * Sets the value of the specified setting.
	 * @param settingName the name of the setting to set.
	 * @param value the new value of the setting.
	 */
	public void set(String settingName, int value)
	{
		if (Gdx.app.getType() == ApplicationType.Desktop)
		{
			settings.setProperty(settingName, Integer.toString(value));
		}
		else
		{
			Preferences pref = Gdx.app.getPreferences(fileName);
			pref.putInteger(settingName, value);
		}
	}
}
