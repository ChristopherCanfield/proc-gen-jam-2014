package com.divergentthoughtsgames.ants.audio;

import com.divergentthoughtsgames.ants.App;

/**
 * Stores the names of the game's audio files. Audio files should be accessed using the AssetManager, 
 * which is accessible via <code>App.assetManager</code>.
 * @author Christopher D. Canfield
 */
public abstract class Audio
{
	public static class Sfx {
	}
	
	public static class Ambient {
		public final String Birds1 = App.paths.Ambient + "34207__cajo__birds-01-Noncommercial.mp3";
		public final String Birds2 = App.paths.Ambient + "53697__inchadney__what-bird.mp3";
		public final String Birds3 = App.paths.Ambient + "123029__cgeffex__evening-birds-mockingbird-short.mp3";
		public final String Birds4 = App.paths.Ambient + "216210__rsilveira-88__birds-02.mp3";
		public final String Birds5 = App.paths.Ambient + "216211__rsilveira-88__bird-01.mp3";
		public final String Birds6 = App.paths.Ambient + "216212__rsilveira-88__birds-04.mp3";
	}
	
	public static class Music {
		public final String VladimirSterzerAnotherStyle = App.paths.Music + "vladimir-sterzer-another-style-jamendo-unlicensed.mp3";
		public final String Newgrounds527762 = App.paths.Music + "527762-newgrounds-noncommercial.mp3";
		public final String SynesthesiaNewgrounds542198 = App.paths.Music + "542198-synesthesia-newgrounds-noncommercial.mp3";
		public final String BurningDesertNewgrounds583089 = App.paths.Music + "583089-burning-desert-newgrounds-noncommercial.mp3";
		public final String ApocalypseNewgrounds490067 = App.paths.Music + "490067_apocalypse_newgrounds_noncommercial.mp3";
		
//		public final String getRandom()
//		{
//			int randInt = MathUtils.random(0, 0);
//			switch (randInt)
//			{
//				case 0:
//					return VladimirSterzerAnotherStyle;
//				default:
//					// TODO (10/14/2014): Once testing is complete, change this to a default song, 
//					// and log the issue.
//					throw new LogicException(
//							"Unable to get random song; MathUtils.random returned value outside of valid range: " + randInt);
//			}
//		}
	}
	
	/** Names of the game's sound effect files. **/
	public static final Sfx sfx = new Sfx();
	
	/** Names of the game's music files. **/
	public static final Music music = new Music();
	
	/** Names of the game's ambient sound files. **/
	public static final Ambient ambient = new Ambient();
}
