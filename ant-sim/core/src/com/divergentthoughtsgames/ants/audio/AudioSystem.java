package com.divergentthoughtsgames.ants.audio;

import java.util.List;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.audio.Music.OnCompletionListener;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.utils.Timer.Task;
import com.divergentthoughtsgames.ants.App;

/**
 * Interface to the game's audio system.
 * TODO (10/14/2014): Implement these methods, and make class concrete.
 * TODO (10/14/2014): Add sound attenuation and clipping.
 *   See the section "Attenuation of sound in air" at www.kayelaby.npl.co.uk/general_physics/2_4/2_4_1.html
 * @author Christopher D. Canfield
 */
public class AudioSystem
{
	private int sfxVolume = 100;
	private int musicVolume = 100;
	private int ambientVolume = 100;
	
	// The music playlist. A list of audio file names, including paths.
	private List<String> playlist;
	
	private Music currentMusic;
	private int currentMusicIndex;
	
	// The list of ambient sound file names, including paths.
	private List<String> ambientSoundsList;
	
	private Music currentAmbient;
	
	private final MusicOnCompletionListener musicOnCompletionListener;
	private final AmbientOnCompletionListener ambientOnCompletionListener;
	
	/**
	 * Constructs the audio system.
	 */
	public AudioSystem()
	{
		this.musicOnCompletionListener = new MusicOnCompletionListener();
		this.ambientOnCompletionListener = new AmbientOnCompletionListener();
	}
	
	/**
	 * Sets the volume of sound effects, from 0 (mute) to 100 (max).
	 * @param volume the volume of sound effects, from 0 (mute) to 100 (max).
	 */
	public void setSoundEffectVolume(int volume)
	{
		sfxVolume = MathUtils.clamp(volume, 0, 100);
	}
	
	/**
	 * Sets the music volume, from 0 (mute) to 100 (max).
	 * @param volume the music volume, from 0 (mute) to 100 (max).
	 */
	public void setMusicVolume(int volume)
	{
		musicVolume = MathUtils.clamp(volume, 0, 100);
		
		if (currentMusic != null)
		{
			currentMusic.setVolume(musicVolume / 100.f);
		}
	}
	
	/**
	 * Sets the ambient sounds volume, from 0 (mute) to 100 (max).
	 * @param volume the ambient sounds volume, from 0 (mute) to 100 (max).
	 */
	public void setAmbientVolume(int volume)
	{
		ambientVolume = MathUtils.clamp(volume, 0, 100);
		
		if (currentAmbient != null)
		{
			currentAmbient.setVolume(ambientVolume / 100.f);
		}
	}
	
	
	////////////////////////////////////
	
	
	/**
	 * Stops all audio.
	 */
	public void stopAll()
	{
		if (currentMusic != null)
		{
			currentMusic.stop();
		}
		
		if (currentAmbient != null)
		{
			currentAmbient.stop();
		}
		
		// TODO (10/24/2014): Should any currently playing sounds be stopped?
	}
	
	/**
	 * Pauses all audio.
	 */
	public void pause()
	{
		if (currentMusic != null)
		{
			currentMusic.pause();
		}
		
		if (currentAmbient != null)
		{
			currentAmbient.pause();
		}
	}
	
	/**
	 * Resumes playing all audio.
	 */
	public void unpause()
	{
		if (currentMusic != null)
		{
			currentMusic.play();
		}
		
		if (currentAmbient != null)
		{
			currentAmbient.play();
		}
	}
	
	
	////////////////////////////////////
	
	
	/**
	 * Sets the music playlist.
	 * @param playlist a list of the music file names (with path). 
	 */
	public void setMusicPlaylist(List<String> playlist)
	{
		if (playlist.isEmpty())
		{
			throw new IllegalArgumentException("Invalid music playlist: The list is empty.");
		}
		
		this.playlist = playlist; 
	}
	
	/**
	 * Starts the game's music, and sets the playlist.
	 * @param playlist a list of the music file names (with path).
	 */
	public void startMusic(List<String> playlist)
	{
		setMusicPlaylist(playlist);
		startMusic();
	}
	
	/**
	 * Starts the game's music. The playlist must be set first.
	 */
	public void startMusic()
	{
		if (playlist == null)
		{
			throw new IllegalStateException("Unable to start music: The playlist is not set.");
		}
		
		if (currentMusic != null)
		{
			currentMusic.stop();
			// TODO (10/21/2014): Unload the music file?
		}
		
		String musicFilePath = playlist.get(0);
		currentMusic = App.assetManager.get(musicFilePath, Music.class);
		currentMusic.play();
		currentMusic.setVolume(musicVolume / 100.f);
		currentMusicIndex = 0;
		
		currentMusic.setOnCompletionListener(musicOnCompletionListener);
	}
	
	/**
	 * Stops the game's music.
	 */
	public void stopMusic()
	{
		if (currentMusic != null)
		{
			currentMusic.stop();
			// TODO (10/21/2014): Unload the music file?
			currentMusic = null;
		}
	}
	
	/**
	 * Plays the next song in the list.
	 */
	public void playNextSong()
	{
		if (currentMusic == null)
		{
			startMusic();
			return;
		}
		
		// TODO (10/21/2014): Unload the music file?
		
		int nextMusicIndex = currentMusicIndex + 1;
		if (nextMusicIndex >= playlist.size())
		{
			nextMusicIndex = 0;
		}
		
		String musicFilePath = playlist.get(nextMusicIndex);
		currentMusic = App.assetManager.get(musicFilePath, Music.class);
		currentMusicIndex = nextMusicIndex;
		currentMusic.setVolume(musicVolume / 100.f);
		currentMusic.play();
		
		currentMusic.setOnCompletionListener(musicOnCompletionListener);
	}
	
	////////////////////////////////////
	
	/**
	 * Plays the specified sound effect.
	 * @param sfx the name of the sound effect to play. The name should be taken from the <code>Audio.sfx</code> 
	 * object: <code>App.audio.play(Audio.sfx.Explosion);</code> 
	 */
	public void play(String sfx)
	{
		Sound sound = App.assetManager.get(sfx, Sound.class);
		
		float pitch = 1;
		float pan = 0;
		sound.play(sfxVolume / 100.f, pitch, pan);
	}
	
	////////////////////////////////////
	
	/**
	 * Sets the ambient sounds playlist.
	 * @param playlist the list of ambient sounds (file names) to play.
	 */
	public void setAmbientPlaylist(List<String> playlist)
	{
		ambientSoundsList = playlist;
	}
	
	/**
	 * Starts playing ambient sounds from the playlist, and sets the playlist.
	 * @param playlist the list of ambient sounds (file names) to play.
	 */
	public void startAmbientSounds(List<String> playlist)
	{
		setAmbientPlaylist(playlist);
		startAmbientSounds();
	}
	
	/**
	 * Starts playing ambient sounds from the playlist.
	 */
	public void startAmbientSounds()
	{
		if (playlist == null)
		{
			throw new IllegalStateException("Unable to start ambient sounds: The playlist is not set.");
		}
		
		playNextAmbientSound();
	}
	
	/**
	 * Stops playing ambient sounds.
	 */
	public void stopAmbientSounds()
	{
		currentAmbient.stop();
	}
	
	/**
	 * Plays a random ambient sound. New sounds continue to play in a random order until <code>stopAmbientSounds</code>
	 * is called.
	 */
	public void playNextAmbientSound()
	{
		if (currentAmbient != null)
		{
			currentAmbient.stop();
		}
		
		int nextIndex = MathUtils.random(0, ambientSoundsList.size() - 1);
		String nextAmbientPath = ambientSoundsList.get(nextIndex);
		currentAmbient = App.assetManager.get(nextAmbientPath, Music.class);
		currentAmbient.setVolume(ambientVolume / 100.f);
		currentAmbient.play();	
		
		currentAmbient.setOnCompletionListener(ambientOnCompletionListener);
	}
	
	
	////////////////////////////////////
	
	/**
	 * Called when a music file has reached its end.
	 * @author Christopher D. Canfield
	 */
	class MusicOnCompletionListener implements OnCompletionListener
	{
		@Override
		public void onCompletion(Music music)
		{
			playNextSong();
			Gdx.app.debug("MusicOnCompletionListener", "playNextSong()");
		}
	}
	
	/**
	 * Called when an ambient sound file has reached its end.
	 * @author Christopher D. Canfield
	 */
	class AmbientOnCompletionListener implements OnCompletionListener
	{
		@Override
		public void onCompletion(Music music)
		{
			float nextAmbientDelay = MathUtils.random(10.f);
			
			App.timer.scheduleTask(new Task() {
				@Override
				public void run()
				{
					playNextAmbientSound();
					Gdx.app.debug("AmbientOnCompletionListener", "playNextAmbientSound()");
				}
			}, nextAmbientDelay);
		}
	}
}
