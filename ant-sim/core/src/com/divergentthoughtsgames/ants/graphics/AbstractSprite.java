package com.divergentthoughtsgames.ants.graphics;

import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.math.Rectangle;

/**
 * Base class for sprites.
 * @author Christopher D. Canfield
 */
abstract class AbstractSprite implements Sprite
{
	// The layer that the sprite is drawn to. Lower layers are drawn below higher layers.
	private final int drawLayer;
	
	/** Whether the sprite is diposed and should be removed from the game. **/
	protected boolean disposed;
	
	/** Whether the sprite has been updated this frame. **/
	protected boolean isUpdated;

	/**
	 * Constructs a new AbstractSprite.
	 * @param drawLayer the layer that the sprite will be drawn to.
	 */
	AbstractSprite(int drawLayer)
	{
		this.drawLayer = drawLayer;
	}
	
	@Override
	public String getTextureName()
	{
		return null;
	}

	@Override
	public int getDrawLayer()
	{
		return drawLayer;
	}

	@Override
	public boolean isWithinCameraBounds(OrthographicCamera camera)
	{
		final float adjustedCameraX = camera.position.x - camera.viewportWidth / 2.f * camera.zoom;
		final float adjustedCameraY = camera.position.y - camera.viewportHeight / 2.f * camera.zoom;
		
		final Rectangle clipBounds = new Rectangle(adjustedCameraX, adjustedCameraY,
				camera.viewportWidth * camera.zoom, camera.viewportHeight * camera.zoom);
		
		if (!isUpdated) update();
		
		return (clipBounds.overlaps(getBoundingRectangle()));
	}

	/**
	 * Called once per frame. This method can be used to track movement, rotation, and so forth.
	 */
	protected abstract void update();
	
	/**
	 * Returns the sprite's bounding rectangle, which is used to determine whether the sprite should 
	 * be drawn or not. 
	 * @return the sprite's bounding rectangle. 
	 */
	protected abstract Rectangle getBoundingRectangle();

	@Override
	public boolean isDisposed()
	{
		return disposed;
	}
}
