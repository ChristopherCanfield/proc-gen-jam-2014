package com.divergentthoughtsgames.ants.graphics;

import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Rectangle;
import com.divergentthoughtsgames.ants.App;
import com.divergentthoughtsgames.ants.world.Ant;

/**
 * A sprite that represents an ant.
 * @author Christopher D. Canfield
 */
public class AntSprite extends AbstractSprite
{
	private static final String textureName = Textures.Ant;
	
	private Ant ant;
	
	private com.badlogic.gdx.graphics.g2d.Sprite libGdxSprite;
	
	/**
	 * Constructs an AntSprite.
	 * @param ant the Ant that this sprite represents.
	 */
	public AntSprite(Ant ant)
	{
		super(DrawLayer.Ants);
		
		this.ant = ant;
		
		libGdxSprite = new com.badlogic.gdx.graphics.g2d.Sprite(
				App.assetManager.get(textureName, Texture.class));
		libGdxSprite.setOriginCenter();
	}
	
	@Override
	public String getTextureName()
	{
		return textureName;
	}

	@Override
	public void draw(SpriteBatch batch, OrthographicCamera camera)
	{
		if (!isUpdated) update();
		
		libGdxSprite.draw(batch);
		//Gdx.app.debug("Draw", "Drawing ant sprite: " + libGdxSprite.getX() + "," + libGdxSprite.getY());
		
		isUpdated = false;
	}
	
	@Override
	protected void update()
	{
		isUpdated = true;
		
		libGdxSprite.setPosition(ant.getBounds().x, ant.getBounds().y);
		libGdxSprite.setRotation(ant.getRotation());
	}
	
	@Override
	protected Rectangle getBoundingRectangle()
	{
		return libGdxSprite.getBoundingRectangle();
	}
}
