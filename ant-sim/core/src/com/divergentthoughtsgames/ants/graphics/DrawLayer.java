package com.divergentthoughtsgames.ants.graphics;

public abstract class DrawLayer
{
	/** The number of draw layers. Draw layers start at zero, and end at DrawLayer.TopLayer. **/
	public static final int LayerCount = DrawLayer.TopLayer + 1;
	
	/** The layer that is drawn last, on top of all other layers. **/
	public static final int TopLayer = 5;
	
	/** The paths layer; used for debugging. **/
	public static final int NavPaths = 1;
	
	/** The navigation nodes layer; used for debugging. **/
	public static final int NavNodes = 0;
	
	/** The ants layer. **/
	public static final int Ants = 4;
	
	/** The pheromone layer. **/
	public static final int Pheromones = 3;
}
