package com.divergentthoughtsgames.ants.graphics;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.maps.tiled.renderers.OrthogonalTiledMapRenderer;
import com.divergentthoughtsgames.ants.Debug;
import com.divergentthoughtsgames.ants.input.CameraController;
import com.divergentthoughtsgames.ants.nav.Node;
import com.divergentthoughtsgames.ants.world.Ant;

/**
 * The graphics subsystem for the game.
 * @author Christopher D. Canfield
 */
public class GraphicsSystem
{
	// Each array index is a draw layer.
	private ArrayList<Sprite>[] layers;
	
	// The draw list for the current frame.
	private ArrayList<Sprite> drawList;

	// The game's camera.
	private OrthographicCamera camera;
	
	private SpriteBatch spriteBatch;
	
	private ArrayList<CameraController> cameraControllers;
	
	// The renderer for the map.
	private OrthogonalTiledMapRenderer mapRenderer;
	
	/**
	 * Creates the Graphics System.
	 */
	@SuppressWarnings({ "unchecked" })
	public GraphicsSystem()
	{
		layers = new ArrayList[DrawLayer.LayerCount];
		drawList = new ArrayList<>();
		
		camera = new OrthographicCamera(Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
		// Move the camera so it doesn't display any off-map space.
		camera.position.set(Gdx.graphics.getWidth() / 2.f, Gdx.graphics.getHeight() / 2.f, 0.f);
		camera.update();
		
		spriteBatch = new SpriteBatch();
		spriteBatch.setProjectionMatrix(camera.combined);
		
		for (int layer = 0; layer < DrawLayer.LayerCount; layer++)
		{
			layers[layer] = new ArrayList<Sprite>();
		}
		
		cameraControllers = new ArrayList<CameraController>();
		
		Gdx.app.debug("Draw", "Camera: " + camera.viewportWidth + "," + camera.viewportHeight);
	}
	
	/**
	 * Adds a sprite to the draw list.
	 * @param sprite the sprite to add.
	 * @return reference to the new sprite.
	 */
	public Sprite addSprite(Sprite sprite)
	{
		layers[sprite.getDrawLayer()].add(sprite);
		return sprite;
	}
	
	/**
	 * Adds an ant sprite to the draw list.
	 * @param ant the underlying Ant that the sprite represents.
	 * @return reference to the new sprite.
	 */
	public Sprite addSprite(Ant ant)
	{
		Gdx.app.debug("Draw", "adding sprite (ant)");
		AntSprite sprite = new AntSprite(ant);
		addSprite(sprite);
		return sprite;
	}
	
	/**
	 * Adds a node sprite to the draw list.
	 * @param node the underlying Node that the sprite represents.
	 * @return reference to the new sprite.
	 */
	public Sprite addSprite(Node node)
	{
		NavNodeSprite sprite = new NavNodeSprite(node);
		addSprite(sprite);
		return sprite;
	}
	
	/**
	 * Adds an edge sprite to the draw list.
	 * @param node1 the start of the edge.
	 * @param node2 the end of the edge.
	 * @return reference to the new sprite.
	 */
	public Sprite addSprite(Node node1, Node node2)
	{
		NavEdgeSprite sprite = new NavEdgeSprite(node1, node2);
		addSprite(sprite);
		return sprite;
	}
	
	/**
	 * Updates the specified sprite's draw layer.
	 * @param sprite the sprite to update.
	 * @param formerDrawLayer the old draw layer of the sprite.
	 */
	public void updateSpriteDrawLayer(Sprite sprite, int formerDrawLayer)
	{
		layers[formerDrawLayer].remove(sprite);
		layers[sprite.getDrawLayer()].add(sprite);
	}
	
	/**
	 * Adds a camera controller to the list.
	 * @param c the camera controller to add.
	 */
	public void addCameraController(CameraController c)
	{
		cameraControllers.add(c);
	}
	
	/**
	 * Sets the Tiled map.
	 * @param map the Tiled map.
	 */
	public void setMap(TiledMap map)
	{
		if (mapRenderer != null)
		{
			mapRenderer.setMap(map);
		}
		else
		{
			mapRenderer = new OrthogonalTiledMapRenderer(map, spriteBatch);
		}
	}
	
	/**
	 * Gets the Tiled map.
	 * @return the Tiled map.
	 */
	public TiledMap getMap()
	{
		return mapRenderer.getMap();
	}
	
	/**
	 * The game camera.
	 * @return reference to the game camera.
	 */
	public OrthographicCamera getCamera()
	{
		return camera;
	}
	
	/**
	 * Renders all of the sprites in the draw list that are within the camera's view.
	 */
	public void render()
	{
		for (CameraController c : cameraControllers)
		{
			c.update();
		}
		
		// Update the camera and set the sprite batch's projection matrix to match the camera's 
		// projection matrix.
		camera.update();
		spriteBatch.setProjectionMatrix(camera.combined);
		
		if (mapRenderer != null)
		{
			mapRenderer.setView(camera);
			
			// Draw the map.
			mapRenderer.render();
		}
		
		// Remove disposed sprites.
		removeDisposedSprites(drawList);
		
		// Rendering algorithm:
		// 	For each rendering layer:
		//		1. Perform clipping: add sprites that are within the camera bounds to the draw list
		//		2. Sort list by texture
		//		3. Render sprites within rendering layer
		final boolean drawNavNodes = Debug.isEnabled(Debug.DrawNavNodes);
		final boolean drawNavPaths = Debug.isEnabled(Debug.DrawNavPaths);
		
		for (int layer = 0; layer < DrawLayer.LayerCount; layer++)
		{
			if (!layers[layer].isEmpty() && !(!drawNavNodes && layer == DrawLayer.NavNodes) && !(!drawNavPaths && layer == DrawLayer.NavPaths))
			{
				// 1. Perform clipping: add sprites that are within the camera bounds to the draw list
				clipSprites(layers[layer], drawList, camera);
				
				if (!drawList.isEmpty())
				{
					// 2. Sort list by texture
					Collections.sort(drawList, new SpriteTextureComparator());
					
					spriteBatch.begin();
					// 3. Render sprites within rendering layer
					for (Sprite sprite : layers[layer])
					{
						sprite.draw(spriteBatch, camera);
					}
					spriteBatch.end();
				}
			}
		}
	}
	
	/**
	 * Removes all disposed sprites from the draw list.
	 * @param spriteList the sprite list.
	 */
	private static void removeDisposedSprites(ArrayList<Sprite> spriteList)
	{
		int numberToRemove = 0;
		for (Sprite sprite : spriteList)
		{
			if (sprite.isDisposed()) numberToRemove++;
		}
		
		if (numberToRemove > 0)
		{
			ArrayList<Sprite> removeList = new ArrayList<Sprite>(numberToRemove);
			int removeCount = 0;
			for (Sprite sprite : spriteList)
			{
				if (sprite.isDisposed())
				{
					removeList.add(sprite);
					removeCount++;
				}
				if (removeCount >= numberToRemove)
				{
					break;
				}
			}
			spriteList.removeAll(removeList);
		}
	}
	
	/**
	 * Adds sprites that are within the camera's bounds to the draw list.
	 * @param spriteList the list of sprites to check.
	 * @param drawList an instantiated ArrayList that will be populated with sprites that are within 
	 * the camera's bounds. This list is cleared when this method is called.
	 * @param camera the game camera.
	 */
	private static void clipSprites(ArrayList<Sprite> spriteList, ArrayList<Sprite> drawList, OrthographicCamera camera)
	{
		drawList.clear();
		for (Sprite sprite : spriteList)
		{
			if (sprite.isWithinCameraBounds(camera))
			{
				drawList.add(sprite);
			}
		}
	}
	
	/**
	 * Camparator that compares two sprites by their texture name.
	 * @author Christopher D. Canfield
	 */
	private static class SpriteTextureComparator implements Comparator<Sprite> 
	{
		@Override
		public int compare(Sprite sprite1, Sprite sprite2)
		{
			return sprite1.getTextureName().compareTo(sprite2.getTextureName());
		}
	}
}
