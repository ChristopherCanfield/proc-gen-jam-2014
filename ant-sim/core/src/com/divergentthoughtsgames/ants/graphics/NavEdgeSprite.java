package com.divergentthoughtsgames.ants.graphics;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Rectangle;
import com.divergentthoughtsgames.ants.App;
import com.divergentthoughtsgames.ants.nav.Node;
import com.divergentthoughtsgames.ants.util.LogicException;

/**
 * Visual representation of a navigation graph edge.
 * @author Christopher D. Canfield
 */
public class NavEdgeSprite extends AbstractSprite
{
	private static final String textureName = Textures.NavEdges;
	
	private com.badlogic.gdx.graphics.g2d.Sprite libGdxSprite;
	
	/**
	 * Constructs a new NavEdgeSprite between <code>node1</code> and <code>node2</code>.
	 * @param node1 the first node that the edge is connected to.
	 * @param node2 the second node that the edge is connected to.
	 */
	public NavEdgeSprite(Node node1, Node node2)
	{
		super(DrawLayer.NavPaths);
		
		Texture texture = App.assetManager.get(textureName, Texture.class);
		TextureRegion region = null;
		int x = 0;
		int y = 0;
		
		// 1: Left
		if (node1.getX() > node2.getX() && node1.getY() == node2.getY())
		{
			Gdx.app.debug("Pathfinding", node1.toString() + " " + node2.toString() + " Left");
			region = new TextureRegion(texture, 0, 59, 64, 5);
			x = node2.getCenterX();
			y = node1.getCenterY();
		}
		// 2: Bottom Left
		else if (node1.getX() > node2.getX() && node1.getY() > node2.getY())
		{
			Gdx.app.debug("Pathfinding", node1.toString() + " " + node2.toString() + " Bottom Left");
			region = new TextureRegion(texture, 128, 0, 64, 64);
			x = node2.getCenterX();
			y = node2.getCenterY();
		}
		// 3: Bottom
		else if (node1.getX() == node2.getX() && node1.getY() > node2.getY())
		{
			Gdx.app.debug("Pathfinding", node1.toString() + " " + node2.toString() + " Bottom");
			region = new TextureRegion(texture, 64, 0, 5, 64);
			x = node1.getCenterX();
			y = node2.getCenterY();
		}
		// 4: Bottom Right
		else if (node1.getX() < node2.getX() && node1.getY() > node2.getY())
		{
			Gdx.app.debug("Pathfinding", node1.toString() + " " + node2.toString() + " Bottom Right");
			region = new TextureRegion(texture, 192, 0, 64, 64);
			x = node1.getCenterX();
			y = node2.getCenterY();
		}
		// 5: Right
		else if (node1.getX() < node2.getX() && node1.getY() == node2.getY())
		{
			Gdx.app.debug("Pathfinding", node1.toString() + " " + node2.toString() + " Right");
			region = new TextureRegion(texture, 0, 59, 64, 5);
			x = node1.getCenterX();
			y = node1.getCenterY();
		}
		// 6: Top Right
		else if (node1.getX() < node2.getX() && node1.getY() < node2.getY())
		{
			Gdx.app.debug("Pathfinding", node1.toString() + " " + node2.toString() + " Top Right");
			region = new TextureRegion(texture, 128, 0, 64, 64);
			x = node1.getCenterX();
			y = node1.getCenterY();
		}
		// 7: Top
		else if (node1.getX() == node2.getX() && node1.getY() < node2.getY())
		{
			Gdx.app.debug("Pathfinding", node1.toString() + " " + node2.toString() + " Top");
			region = new TextureRegion(texture, 64, 0, 5, 64);
			x = node1.getCenterX();
			y = node1.getCenterY();
		}
		// 8: Top Left
		else if (node1.getX() > node2.getX() && node1.getY() < node2.getY())
		{
			Gdx.app.debug("Pathfinding", node1.toString() + " " + node2.toString() + " Top Left");
			region = new TextureRegion(texture, 192, 0, 64, 64);
			x = node2.getCenterX();
			y = node1.getCenterY();
		}
		else
		{
			throw new LogicException("Unknown edge direction: " + node1.toString() + " " + node2.toString());
		}
		
		libGdxSprite = new com.badlogic.gdx.graphics.g2d.Sprite(region);
		libGdxSprite.setPosition(x, y);
		
		isUpdated = true;
	}
	
	@Override
	public String getTextureName()
	{
		return textureName;
	}

	@Override
	public void draw(SpriteBatch batch, OrthographicCamera camera)
	{
		libGdxSprite.draw(batch);
	}
	
	@Override
	protected void update()
	{
	}
	
	@Override
	protected Rectangle getBoundingRectangle()
	{
		return libGdxSprite.getBoundingRectangle();
	}
	
	/**
	 * Disposes the NavEdgeSprite.
	 */
	public void dispose()
	{
		disposed = true;
	}
}
