package com.divergentthoughtsgames.ants.graphics;

import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Rectangle;
import com.divergentthoughtsgames.ants.App;
import com.divergentthoughtsgames.ants.nav.Node;

/**
 * A sprite representing a navigation node.
 * @author Christopher D. Canfield
 */
public class NavNodeSprite extends AbstractSprite
{
	private static final String textureName = Textures.NavNode;
	
	private com.badlogic.gdx.graphics.g2d.Sprite libGdxSprite;
	
	private final Node node;
	
	/**
	 * Constructs a new NavNodeSprite, which is a sprite that represents a navigation node.
	 * @param node the navigation node that this sprite represents.
	 */
	public NavNodeSprite(Node node)
	{
		super(DrawLayer.NavNodes);
		
		this.node = node;
		
		libGdxSprite = new com.badlogic.gdx.graphics.g2d.Sprite(
				App.assetManager.get(textureName, Texture.class));
		libGdxSprite.setPosition(node.getX(), node.getY());
		
		isUpdated = true;
	}
	
	@Override
	public String getTextureName()
	{
		return textureName;
	}

	@Override
	public void draw(SpriteBatch batch, OrthographicCamera camera)
	{
		if (node.isPassable())
		{
			libGdxSprite.draw(batch);
		}
	}
	
	@Override
	protected void update()
	{
	}
	
	@Override
	protected Rectangle getBoundingRectangle()
	{
		return libGdxSprite.getBoundingRectangle();
	}
}
