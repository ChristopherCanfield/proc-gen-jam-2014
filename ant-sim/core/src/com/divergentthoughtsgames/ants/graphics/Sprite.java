package com.divergentthoughtsgames.ants.graphics;

import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

/**
 * A drawable object.
 * @author Christopher D. Canfield
 */
public interface Sprite
{
	/**
	 * Gets the name of the sprite's texture. Used for texture sorting.
	 * @return the name of the sprite's texture.
	 */
    public String getTextureName();
    
    /**
     * Gets the sprite's draw layer.
     * @return the sprite's draw layer. Lower layers are drawn below later layers.
     */
    public int getDrawLayer();
    
    /**
     * Returns true if this sprite is within the camera's bounds.
     * @param camera the game camera.
     * @return true if this sprite is within the camera's bounds, or false otherwise.
     */
    public boolean isWithinCameraBounds(OrthographicCamera camera);
    
    /**
     * Draws this sprite.
     * @param batch the SpriteBatch, which has had its begin() method called.
     * @param camera the game camera.
     */
    public void draw(SpriteBatch batch, OrthographicCamera camera);
    
    /**
     * Specifies whether the sprite has been disposed and should be removed.
     * @return true if the sprite has been disposed and should be removed, or false otherwise.
     */
    public boolean isDisposed();
}
