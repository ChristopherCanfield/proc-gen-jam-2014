package com.divergentthoughtsgames.ants.graphics;

import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.assets.loaders.TextureLoader.TextureParameter;
import com.badlogic.gdx.graphics.Texture;
import com.divergentthoughtsgames.ants.App;

/**
 * Stores the names of the textures. Textures should be accessed using the AssetManager, which is 
 * accessible via <code>App.assetManager</code>.
 * @author Christopher D. Canfield
 */
public abstract class Textures
{
	public static final String Ant = App.paths.Textures + "ant.png";
	public static final String AntFoodPile = App.paths.Textures + "ant-food-pile.png";
	public static final String Rock = App.paths.Textures + "rock.png";
	public static final String Grass = App.paths.Textures + "grass.jpg";
	public static final String Anthill = App.paths.Textures + "anthill.png";
	
	public static final String NavNode = App.paths.Textures + "nav-cell.png";
	public static final String NavEdges = App.paths.Textures + "nav-edges.png";
	
	/**
	 * Queues all textures into the asset manager for loading.
	 * @param assetManager the game's asset manager.
	 */
	public static void loadAll(AssetManager assetManager)
	{
		TextureParameter textureParam = new TextureParameter();
		textureParam.genMipMaps = true;
		
		App.assetManager.load(Textures.Ant, Texture.class, textureParam);
		App.assetManager.load(Textures.AntFoodPile, Texture.class, textureParam);
		App.assetManager.load(Textures.Anthill, Texture.class, textureParam);
		App.assetManager.load(Textures.Grass, Texture.class, textureParam);
		App.assetManager.load(Textures.Rock, Texture.class, textureParam);
		
		App.assetManager.load(Textures.NavNode, Texture.class, textureParam);
		App.assetManager.load(Textures.NavEdges, Texture.class, textureParam);
	}
}
