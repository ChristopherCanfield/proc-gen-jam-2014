package com.divergentthoughtsgames.ants.input;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.divergentthoughtsgames.ants.App;

public abstract class CameraController
{
	private static final float maxZoomIn = 0.75f;
	private static final float maxZoomOut = 10;
	
	private OrthographicCamera camera;

	protected CameraController(OrthographicCamera camera)
	{
		this.camera = camera;
	}
	
	protected void move(float xOffset, float yOffset)
	{
		if (xOffset != 0)
		{
			camera.position.x = calculateCameraX(camera, xOffset);
		}
		
		if (yOffset != 0)
		{
			camera.position.y = calculateCameraY(camera, yOffset);
		}
	}
	
	protected void zoom(float zoomFactor)
	{
		final float maxZoomWidth = App.world.getWidthPixels() / camera.viewportWidth;
		final float maxZoomHeight = App.world.getHeightPixels() / camera.viewportHeight;
		final float newZoom = camera.zoom * zoomFactor;
		
		Gdx.app.debug("Camera", "New zoom: " + newZoom);
		if (newZoom > maxZoomWidth)
		{
			camera.zoom = maxZoomWidth;
			Gdx.app.debug("Camera", "Max Zoom width reached.");
		}
		else if (newZoom > maxZoomHeight)
		{
			camera.zoom = maxZoomHeight;
			Gdx.app.debug("Camera", "Max zoom height reached.");
		}
		else if (newZoom < maxZoomIn)
		{
			camera.zoom = maxZoomIn;
			Gdx.app.debug("Camera", "Max zoom in reached.");
		}
		else if (newZoom > maxZoomOut)
		{
			camera.zoom = maxZoomOut;
			Gdx.app.debug("Camera", "Max zoom out reached.");
		}
		else
		{
			camera.zoom *= zoomFactor;
		}
		
		move(-0.0001f, -0.0001f);
		move(0.0001f, 0.0001f);
	}
	
	protected void resetZoom()
	{
		camera.zoom = 1.f;
	}
	
	public abstract void update();
	
	private static float calculateCameraX(OrthographicCamera camera, float xOffset)
	{
//		Gdx.app.debug("Camera", "Camera x: " + camera.position.x);
//		Gdx.app.debug("Camera", "Camera width: " + camera.viewportWidth / 2.f);
		
		float newCameraX = 0;
		final float cameraHalfWidth = camera.viewportWidth / 2.f * camera.zoom;
		if ((camera.position.x - cameraHalfWidth) <= 0 && xOffset < 0)
		{
			newCameraX = cameraHalfWidth;
		}
		else if (camera.position.x + cameraHalfWidth >= App.world.getWidthPixels() &&
				xOffset > 0)
		{
			newCameraX = App.world.getWidthPixels() - cameraHalfWidth;
		}
		else
		{
			newCameraX = camera.position.x + xOffset;
		}

		return newCameraX;
	}

	private static float calculateCameraY(OrthographicCamera camera, float yOffset)
	{
		float newCameraY = 0;
		final float cameraHalfHeight = camera.viewportHeight / 2.f * camera.zoom;
		if ((camera.position.y - cameraHalfHeight) <= 0 && yOffset < 0)
		{
			newCameraY = cameraHalfHeight;
		}
		else if (camera.position.y + cameraHalfHeight >= App.world.getHeightPixels() &&
				yOffset > 0)
		{
			newCameraY = App.world.getHeightPixels() - cameraHalfHeight;
		}
		else
		{
			newCameraY = camera.position.y + yOffset;
		}

		return newCameraY;
	}
}
