package com.divergentthoughtsgames.ants.input;

import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.InputAdapter;
import com.divergentthoughtsgames.ants.SavedGame;

/**
 * Processes system control keys, such as quick saving and loading, screenshot capturing, and the like.
 * @author Christopher D. Canfield
 */
public class SystemKeyProcessor extends InputAdapter
{
	@Override
	public boolean keyDown(int keycode)
	{
		return false;
	}

	@Override
	public boolean keyUp(int keycode)
	{
		switch (keycode)
		{
		// Quick save.
		case Keys.F6:
			SavedGame savedGame = new SavedGame();
			savedGame.serialize("quicksave");
			break;
		// Quick load.
		case Keys.F9:
			SavedGame.deserialize("quicksave");
			break;
		default:
			return false;
		}
		
		return false;
	}
}
