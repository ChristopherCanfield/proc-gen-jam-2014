package com.divergentthoughtsgames.ants.input;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.input.GestureDetector.GestureListener;
import com.badlogic.gdx.math.Vector2;
import com.divergentthoughtsgames.ants.App;

public class TouchCameraController extends CameraController implements GestureListener 
{
	private double tapLastZoomTime = 0; 	
	
	public TouchCameraController(OrthographicCamera camera)
	{
		super(camera);
	}

	@Override
	public boolean touchDown(float x, float y, int pointer, int button)
	{
		return false;
	}

	@Override
	public boolean tap(float x, float y, int count, int button)
	{
		return false;
	}

	@Override
	public boolean longPress(float x, float y)
	{
		return false;
	}

	@Override
	public boolean fling(float velocityX, float velocityY, int button)
	{
		return false;
	}

	@Override
	public boolean pan(float x, float y, float deltaX, float deltaY)
	{
		Gdx.app.debug("Touch", "Pan called: " + deltaX + ", " + deltaY + " (time: " + App.getGameTime() + ")");
		move(deltaX, deltaY);
		return false;
	}

	@Override
	public boolean panStop(float x, float y, int pointer, int button)
	{
		return false;
	}

	@Override
	public boolean zoom(float initialDistance, float distance)
	{
		if (tapLastZoomTime - App.getGameTime() != 0)
		{
			final float amount = initialDistance - distance;
			final float zoomFactor = (amount < 0) ? 1.005f : 0.995f;
	
			Gdx.app.debug("Touch", "Touch called. Zoom factor: " + zoomFactor + " (time: " + App.getGameTime() + ")");
			zoom(zoomFactor);
			
			tapLastZoomTime = App.getGameTime();
		}
		
		return false;
	}

	@Override
	public boolean pinch(Vector2 initialPointer1, Vector2 initialPointer2, Vector2 pointer1, Vector2 pointer2)
	{
		return false;
	}

	@Override
	public void update()
	{
	}
}
