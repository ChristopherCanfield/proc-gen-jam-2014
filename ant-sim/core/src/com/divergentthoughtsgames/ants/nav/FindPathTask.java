package com.divergentthoughtsgames.ants.nav;

import java.util.concurrent.Callable;

import com.divergentthoughtsgames.ants.App;

/**
 * Finds a path to the specified node.
 * @author Christopher D. Canfield
 */
public class FindPathTask implements Callable<Path>
{
	private Node startNode;
	private Node targetNode;
	
	/**
	 * Constructs a FindPathTask that finds a path between the start node and a random end node. 
	 * @param start the start node in the path.
	 */
	public FindPathTask(Node start)
	{
		startNode = start;
	}
	
	/**
	 * Constructs a FindPathTask that finds a path between the start node and the target node.
	 * @param start the start node in the path.
	 * @param target the target node for the path.
	 */
	public FindPathTask(Node start, Node target)
	{
		startNode = start;
		targetNode = target;
	}

	@Override
	public Path call() throws Exception
	{
		NavMap navMap = App.world.getNavMap();
		if (targetNode == null)
		{
			targetNode = navMap.getRandomNode(startNode);
		}
		
		Path path = new Path(Search.aStar(startNode, targetNode, StraightLineHeuristic.getInstance()), targetNode);
		return path;
	}
	
}
