/*
 * Christopher D. Canfield
 * Divergent Thoughts Games
 *           2014
 */
package com.divergentthoughtsgames.ants.nav;

import com.badlogic.gdx.math.MathUtils;

/**
 * The navigation map.
 * @author Christopher D. Canfield
 */
public class NavMap
{
	// The navigation list.
	private final Node[] navList;
	
	// The navigation graph, in [column][row] order.
	private Node[][] navGraph;
	
//	/**
//	 * Returns the node at the specified x/y location.
//	 * @param map
//	 * @param worldX
//	 * @param worldY
//	 * @return
//	 */
//	public static Node getNode(NavMap map, int worldX, int worldY)
//	{
//		Node[][] navGraph = map.navGraph;
//		return navGraph[worldX / Node.SIZE][worldY / Node.SIZE];
//	}
	
	/**
	 * Constructs a navigation map.
	 * @param navList the navigation nodes, in a list.
	 * @param navGraph the navigation nodes, in a 2D array.
	 */
	public NavMap(Node[] navList, Node[][] navGraph)
	{
		this.navList = navList;
		this.navGraph = navGraph;
	}
	
	/**
	 * Returns the node located at the specified x,y position.
	 * @param worldX the x position of the node, in world units.
	 * @param worldY the y position of the node, in world units.
	 * @return the node located at the specified x,y position.
	 */
	public Node getNode(int worldX, int worldY)
	{
		return navGraph[worldX / Node.Size][worldY / Node.Size];
	}
	
	/**
	 * Returns the node located at the specified index.
	 * @param column the node's column index.
	 * @param row the node's row index.
	 * @return the node located at the specified index.
	 */
	public Node getNodeByIndex(int column, int row)
	{
		return navGraph[column][row];
	}
	
	/**
	 * Gets a reference to the navigation node list.
	 * @return a reference to the navigation node list.
	 */
	public Node[] getNavList()
	{
		return navList;
	}
	
	/**
	 * Gets a random passable node from the navigation graph.
	 * @return a random passable node.
	 */
	public Node getRandomNode()
	{
		int columns = navGraph.length - 1;
		int rows = navGraph[0].length - 1;
		
		while (true)
		{
			int column = MathUtils.random(columns);
			int row = MathUtils.random(rows);
			if (navGraph[column][row].isPassable())
			{
				return navGraph[column][row];
			}
		}
	}
	
	/**
	 * Gets a random passable node from the navigation graph, while ensuring that the random node
	 * is not the excludedNode.
	 * @param excludedNode a node that won't be returned.
	 * @return a random passable node.
	 */
	public Node getRandomNode(Node excludedNode)
	{
		Node node = null;
		while (node == null || node == excludedNode)
		{
			node = getRandomNode();
		}
		return node;
	}
	
//	public int getColumns()
//	{
//		return width;
//	}
//	
//	public int getRows()
//	{
//		return height;
//	}
}
