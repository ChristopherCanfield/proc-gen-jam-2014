package com.divergentthoughtsgames.ants.nav;

/**
 * Interface for A* search heuristics.
 * @author Christopher D. Canfield
 */
public interface SearchHeuristic
{
	/**
	 * Calculates the distance cost between two nodes.
	 * @param start The start node.
	 * @param end The end node.
	 * @return the distance cost between two nodes. 
	 */
	public double cost(Node start, Node end);
}
