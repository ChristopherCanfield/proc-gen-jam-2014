package com.divergentthoughtsgames.ants.util;

/**
 * Signifies that a file problem was encountered.
 * @author Christopher D. Canfield
 */
public class FileException extends GameException
{
	private static final long serialVersionUID = 1L;

	/**
	 * Constructs a FileException with the specified detail message.
	 * @param message the detail message. 
	 */
	public FileException(String message)
	{
		super(message);
	}
	
	/**
	 * Constructs a new exception with the specified detail message and cause.
	 * @param message the detail message.
	 * @param cause the exception's cause.
	 */
	public FileException(String message, Throwable cause)
	{
		super(message, cause);
	}
}
