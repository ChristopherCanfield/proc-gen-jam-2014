/*
 * Christopher D. Canfield
 * Divergent Thoughts Games
 *           2014
 */
package com.divergentthoughtsgames.ants.util;

import com.badlogic.gdx.math.MathUtils;

public abstract class GameMath
{
	/**
	 * Provides the angle needed to face the specified point, in radians.
	 * @param point1x the x coordinate for point 1.
	 * @param point1y the y coordinate for point 1.
	 * @param point2x the x coordinate for point 2.
	 * @param point2y the y coordinate for point 2.
	 * @return the angle needed to face the specified point, in radians.
	 */
	public static float angleToFace(int point1x, int point1y, int point2x, int point2y)
	{
		int deltaY = point1y - point2y;
		int deltaX = point1x - point2x;
		return MathUtils.atan2(deltaY, deltaX);
	}
	
	/**
	 * Returns true if num is between min and max, inclusive.
	 * @param num the number to check.
	 * @param min the minimum value to check against.
	 * @param max the maximum value to check against.
	 * @return true if num is between min and max, inclusive.
	 */
	public static boolean between(float num, float min, float max)
	{
		return (num >= min && num <= max);
	}
}
