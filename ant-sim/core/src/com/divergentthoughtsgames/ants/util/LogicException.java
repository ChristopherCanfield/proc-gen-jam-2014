package com.divergentthoughtsgames.ants.util;

/**
 * Signals that an application logic error has occurred.
 * @author Christopher D. Canfield
 */
public class LogicException extends GameException
{
	private static final long serialVersionUID = 1L;

	/**
	 * Constructs a LogicException with the specified detail message. Logic exceptions represent 
	 * application logic errors.
	 * @param message the detail message. 
	 */
	public LogicException(String message)
	{
		super(message);
	}
	
	/**
	 * Constructs a new LogicException with the specified detail message and cause. Logic exceptions 
	 * represent application logic errors.
	 * @param message the detail message.
	 * @param cause the exception's cause.
	 */
	public LogicException(String message, Throwable cause)
	{
		super(message, cause);
	}
}
