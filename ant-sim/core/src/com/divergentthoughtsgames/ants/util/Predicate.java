package com.divergentthoughtsgames.ants.util;

/**
 * The Java 8 Predicate interface. Added so it can be used in previous versions.
 * @author BU CS673 - Clone Productions
 * @param <T> The type of the object given to the <code>test(T)</code> method.
 */
public interface Predicate<T>
{
	/**
	 * Returns true if the predicate is true.
	 * @param object the object to test against.
	 * @return true if the predicate is true.
	 */
	public boolean test(T object);
}
