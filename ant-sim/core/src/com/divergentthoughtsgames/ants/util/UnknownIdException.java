package com.divergentthoughtsgames.ants.util;

/**
 * Signifies that an Unknown ID was encountered.
 * @author Christopher D. Canfield
 */
public class UnknownIdException extends GameException
{
	private static final long serialVersionUID = 1L;

	/**
	 * Constructs an UnknownIdException with the specified detail message.
	 * @param message the detail message. 
	 */
	public UnknownIdException(String message)
	{
		super(message);
	}
	
	/**
	 * Constructs a new exception with the specified detail message and cause.
	 * @param message the detail message.
	 * @param cause the exception's cause.
	 */
	public UnknownIdException(String message, Throwable cause)
	{
		super(message, cause);
	}
}
