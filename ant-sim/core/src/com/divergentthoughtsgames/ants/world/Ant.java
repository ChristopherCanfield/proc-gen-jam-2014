package com.divergentthoughtsgames.ants.world;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.ObjectStreamException;
import java.io.Serializable;
import java.io.StreamCorruptedException;
import java.util.LinkedList;
import java.util.Queue;
import java.util.UUID;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.divergentthoughtsgames.ants.App;
import com.divergentthoughtsgames.ants.Debug;
import com.divergentthoughtsgames.ants.graphics.NavEdgeSprite;
import com.divergentthoughtsgames.ants.nav.FindPathTask;
import com.divergentthoughtsgames.ants.nav.Node;
import com.divergentthoughtsgames.ants.nav.Path;

/**
 * The main unit type. Ants are owned by ant colonies, and are serializable.
 * @author Christopher D. Canfield
 */
public class Ant implements Serializable
{
	private static final long serialVersionUID = 1L;

	// The ant's unique id.
	private UUID id;
	
	
	// *** Movement variables ***

	private float maxSpeed;

	private Vector2 velocity;
	
	private float acceleration;
	
	private float deceleration;
	
	
	// *** State variables ***
	
	private int maxHunger;
	
	private float hunger;
	
	private int maxHealth;
	
	private float health;
	
	private int maxAge;
	
	private float age;
	
	
	// *** Location & size variables ***
	
	// X: left; Y: bottom
	private Rectangle rect;
	
	private static final int width = 32;
	
	private static final int height = 32;
	
	// The ant's rotation. Zero degrees points to the right.
	private float rotation;
	
	
	// *** Pathfinding variables ***
	
//	private Node targetNode;
	
	// Used for serialization and deserialization.
	private Vector2 target;
	
	private transient Node nextNode;
	
	private transient Queue<Node> path;
	
	private transient Queue<NavEdgeSprite> pathSprites;
	
	private transient Future<Path> pathfindingTask;
	
	// true if the ant is disposed and should be removed, or false otherwise.
	// Transient because only non-disposed ants are serialized to disk.
	private transient boolean disposed;
	
	/**
	 * Constructs a new ant with a unique id.
	 */
	public Ant()
	{
		this(UUID.randomUUID());
	}
	
	/**
	 * Constructs a new ant with the specified unique id.
	 * @param id the ant's unique id.
	 */
	public Ant(UUID id)
	{
		rect = new Rectangle(0, 0, width, height);
		this.id = id;
		
		App.graphics.addSprite(this);
	}
	
	/**
	 * Updates the Ant. Must be called once per tick.
	 */
	public void update()
	{
		if (path == null || path.isEmpty())
		{
			if (pathfindingTask == null)
			{
				if (target != null)
				{
					Node targetNode = App.world.getNodeByIndex((int)target.x, (int)target.y);
					pathfindingTask = App.taskQueue.submit(new FindPathTask(getNode(), targetNode));
				}
				else
				{
					//pathfindingTask = App.taskQueue.submit(new FindPathTask(getNode(), App.world.getNavMap().getNode(28 * Node.Size, 5 * Node.Size)));
					pathfindingTask = App.taskQueue.submit(new FindPathTask(getNode()));
				}
				Gdx.app.debug("Pathfinding", "Pathfinding task submitted. Start node: " + getNode().toString());
			}
			else if (pathfindingTask.isDone())
			{
				try
				{
					Node targetNode = pathfindingTask.get().getTargetNode();
					target = new Vector2(targetNode.getColumnIndex(), targetNode.getRowIndex());
					
					path = pathfindingTask.get().getNodes();
					
					// Create the navigation path, if drawing nav paths is enabled. 
					if (Debug.isEnabled(Debug.DrawNavPaths))
					{
						pathSprites = new LinkedList<NavEdgeSprite>();
						Node previousNode = null;
						for (Node node : path)
						{
							if (previousNode != null)
							{
								NavEdgeSprite sprite = (NavEdgeSprite)App.graphics.addSprite(previousNode, node);
								pathSprites.add(sprite);
							}
							previousNode = node;
						}
					}
					
					// Remove the first node in the path, since the ant is already on it.
					path.poll();
					nextNode = path.poll();
					
					Gdx.app.debug("Pathfinding", "Path found. Nodes: " + path.size());
					Gdx.app.debug("Pathfinding", "First node: " + nextNode.toString());
				}
				catch (InterruptedException | ExecutionException e)
				{
					Gdx.app.error("Pathfinding", "Pathfinding exception: " + e.getMessage());
					path = null;
					pathfindingTask = null;
				}
			}
		}
	}
	
	/**
	 * Returns a clone of the ant's bounding box. It is safe to modify this rectangle.
	 * @return a clone of the ant's bounding box.
	 */
	public Rectangle getBounds()
	{
		return new Rectangle(rect);
	}
	
	/**
	 * Sets the Ant's position.
	 * @param x the ant's new x position.
	 * @param y the ant's new y position.
	 * @return reference to the ant.
	 */
	public Ant setPosition(int x, int y)
	{
		rect.x = x;
		rect.y = y;
		return this;
	}
	
	/**
	 * Returns the ant's x position (left side of ant).
	 * @return the ant's x position (left side of ant).
	 */
	public float getX()
	{
		return rect.x;
	}
	
	/**
	 * Returns the ant's y position (bottom of ant).
	 * @return the ant's y position (bottom of ant).
	 */
	public float getY()
	{
		return rect.y;
	}
	
	/**
	 * The ant's center x point.
	 * @return the ant's center x point.
	 */
	public float getCenterX()
	{
		return rect.x + (rect.width / 2.f);
	}
	
	/**
	 * The ant's center y point.
	 * @return the ant's center y point.
	 */
	public float getCenterY()
	{
		return rect.y + (rect.height / 2.f);
	}
	
	/**
	 * Returns the node that this Ant is currently on.
	 * @return the node that this Ant is currently on.
	 */
	private Node getNode()
	{
		return App.world.getNavMap().getNode((int)getCenterX(), (int)getCenterY());
	}
	
	/**
	 * Gets the ant's rotation. Zero degrees points to the right.
	 * @return the ant's rotation.
	 */
	public float getRotation()
	{
		return rotation;
	}
	
	/**
	 * Specifies whether the ant has been disposed, and should be removed from the game.
	 * @return true if the ant has been disposed, or false otherwise.
	 */
	public boolean isDisposed()
	{
		return disposed;
	}
	
	
	
	////////// Serialization & deserialization methods //////////
	
	private void writeObject(ObjectOutputStream out) throws IOException
	{
		if (!disposed)
		{
			out.defaultWriteObject();
		}
	}
	
	private void readObject(ObjectInputStream in) throws IOException, ClassNotFoundException
	{
		in.defaultReadObject();
		disposed = false;
		
		App.graphics.addSprite(this);
	}
	
	@SuppressWarnings({ "static-method", "unused" })
	private void readObjectNoData() throws ObjectStreamException
	{
		// TODO (11/1/2014): This may not be needed - look into this further.
		throw new StreamCorruptedException();
	}
}
