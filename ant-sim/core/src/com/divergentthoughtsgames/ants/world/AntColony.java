package com.divergentthoughtsgames.ants.world;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.UUID;

import com.divergentthoughtsgames.ants.App;

/**
 * Owns all ants within a colony. Colonies are owned by the World instance, and are serializable.
 * @author Christopher D. Canfield
 */
public class AntColony implements Serializable
{
	private static final long serialVersionUID = 1L;

	// The ant colony's unique id.
	private int id;
	
	// The list of all ants in the colony.
	private ArrayList<Ant> ants;
	
	// The colony's name.
	private String name;
	
	/**
	 * Creates a new AntColony with the specified name and an unused ID.
	 * @param name the new ant colony's name.
	 */
	public AntColony(String name)
	{
		this(name, App.world.getUnusedColonyId());
	}
	
	/**
	 * Creates a new AntColony with the specified name and ID.
	 * @param name the new ant colony's name.
	 * @param id the new ant colony's id.
	 */
	public AntColony(String name, int id)
	{
		this.ants = new ArrayList<Ant>();
		this.name = name;
		this.id = id;
	}
	
	/**
	 * Gets the name of the ant colony.
	 * @return the name of the ant colony.
	 */
	public String getName()
	{
		return name;
	}
	
	/**
	 * Gets the colony's unique id.
	 * @return the colony's unique id.
	 */
	public int getId()
	{
		return id;
	}
	
	/**
	 * Must be called once per tick. Calls update on all ants owned by this colony.
	 */
	public void update()
	{
		for (Ant ant : ants)
		{
			ant.update();
		}
	}
	
	/**
	 * Adds an ant to this colony.
	 * @param antId the ant's unique id.
	 * @return reference to the new ant.
	 */
	public Ant addAnt(UUID antId)
	{
		// TODO (cdc - 10/8/2014): set the ant's position?
		Ant ant = new Ant(antId);
		ants.add(ant);
		return ant;
	}
}
