package com.divergentthoughtsgames.ants.world;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.UUID;

import com.divergentthoughtsgames.ants.App;
import com.divergentthoughtsgames.ants.nav.NavMap;
import com.divergentthoughtsgames.ants.nav.Node;
import com.divergentthoughtsgames.ants.util.UnknownIdException;

/**
 * The game world.
 * @author Christopher D. Canfield
 */
public class World implements Serializable
{	
	private static final long serialVersionUID = 1L;

	private ArrayList<AntColony> antColonies;
	
	private transient NavMap navMap;
	
	private int widthPixels;
	private int heightPixels;
	
	private int widthTiles;
	private int heightTiles;
	
	/**
	 * Constructs the game world, with the specified width and height in tiles.
	 * @param widthTiles the width of the game world, in tiles.
	 * @param heightTiles the height of the game world, in tiles.
	 */
	public World(int widthTiles, int heightTiles)
	{
		antColonies = new ArrayList<AntColony>();
		
		this.widthTiles = widthTiles;
		this.heightTiles = heightTiles;
		
		this.widthPixels = (int)(widthTiles * App.PixelsPerTile);
		this.heightPixels = (int)(heightTiles * App.PixelsPerTile);
	}
	
	/**
	 * Sets the world's navigation map.
	 * @param navMap the world's navigation map.
	 */
	public void setNavMap(NavMap navMap)
	{
		this.navMap = navMap;
	}
	
	/**
	 * Returns a reference to the navigation map.
	 * @return a reference to the navigation map.
	 */
	public NavMap getNavMap()
	{
		return navMap;
	}
	
	/**
	 * Returns the node located at the specified index.
	 * @param column the node's column index.
	 * @param row the node's row index.
	 * @return the node located at the specified index.
	 */
	public Node getNodeByIndex(int column, int row)
	{
		return navMap.getNodeByIndex(column, row);
	}
	
	/**
	 * Must be called once per tick. Calls update() on all ant colonies.
	 */
	public void update()
	{
		for (AntColony colony : antColonies)
		{
			colony.update();
		}
	}
	
	/**
	 * Adds an ant to the specified colony.
	 * @param antId the ant's unique identifier.
	 * @param colonyId the id of the ant colony that owns the new ant.
	 * @return reference to the new ant.
	 */
	public Ant addAnt(UUID antId, int colonyId)
	{
		for (AntColony colony : antColonies)
		{
			if (colony.getId() == colonyId)
			{
				return colony.addAnt(antId);
			}
		}
		
		throw new UnknownIdException("Unknown colony ID: " + colonyId);
	}
	
	/**
	 * Adds an ant to the specified colony. The ant's ID is generated from <code>UUID.randomUUID()</code>.
	 * @param colonyId the id of the ant colony that owns the new ant.
	 * @return reference to the new ant.
	 */
	public Ant addAnt(int colonyId)
	{
		return addAnt(UUID.randomUUID(), colonyId);
	}
	
	/**
	 * Creates a new AntColony with the specified name and ID, and adds it to the world.
	 * @param name the new ant colony's name.
	 * @param id the new ant colony's id.
	 * @return reference to the new colony.
	 */
	public AntColony addColony(String name, int id)
	{
		AntColony colony = new AntColony(name, id);
		antColonies.add(colony);
		return colony;
	}
	
	/**
	 * Creates a new AntColony with the specified name.
	 * @param name the new ant colony's name.
	 * @return reference to the new colony.
	 */
	public AntColony addColony(String name)
	{
		AntColony colony = new AntColony(name);
		antColonies.add(colony);
		return colony;
	}
	
	/**
	 * Adds the specified colony.
	 * @param colony the colony to add.
	 * @return reference to the colony.
	 */
	public AntColony addColony(AntColony colony)
	{
		antColonies.add(colony);
		return colony;
	}
	
	/**
	 * Returns an unused colony ID.
	 * @return an unused colony ID.
	 */
	public int getUnusedColonyId()
	{
		// Assumes dead colonies are not removed from the list.
		return antColonies.size();
	}

	/**
	 * Returns the world's width, in pixels.
	 * @return the world's width, in pixels.
	 */
	public int getWidthPixels()
	{
		return widthPixels;
	}
	
	/**
	 * Returns the world's height, in pixels.
	 * @return the world's height, in pixels.
	 */
	public int getHeightPixels()
	{
		return heightPixels;
	}
	
	/**
	 * Gets the width of the world in tiles.
	 * @return the width of the world in tiles.
	 */
	public int getWidthTiles()
	{
		return widthTiles;
	}
	
	/**
	 * Gets the height of the world in tiles.
	 * @return the height of the world in tiles.
	 */
	public int getHeightTiles()
	{
		return heightTiles;
	}
}
