package com.divergentthoughtsgames.ants;

import com.badlogic.gdx.Application.ApplicationType;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.divergentthoughtsgames.ants.world.Ant;
import com.divergentthoughtsgames.ants.world.AntColony;
import com.divergentthoughtsgames.ants.world.World;

/**
 * A test state that tests out the game functionality.
 * @author Christopher D. Canfield
 */
public class GameTestState implements Screen
{

	public GameTestState()
	{
		App.initialize();
		App.debugSettings = (Debug.DrawNavNodes + Debug.DrawNavPaths);
		
		MapLoader mapLoader = new MapLoader();
		World world = mapLoader.load("test-map");
		App.setWorld(world);
	}

	@Override
	public void render(float delta)
	{
		if (!App.isPaused())
		{
			Gdx.gl.glClearColor(0.85098f, 1, 0.713725f, 1);
			Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
			
			App.update();
			App.world.update();
			App.graphics.render();
		}
		
	}

	@Override
	public void resize(int width, int height)
	{
	}

	@Override
	public void show()
	{
		Gdx.app.debug("Lifecycle", "[GameTestState] Show called");
		
		// Start music and ambient sounds.
		App.audio.startMusic();
		App.audio.startAmbientSounds();
		
		AntColony colony = App.world.addColony("TestColony");
		Ant ant = App.world.addAnt(colony.getId());
		ant.setPosition(100, 100);
	}

	@Override
	public void hide()
	{
		Gdx.app.debug("Lifecycle", "[GameTestState] Hide called");
	}

	@Override
	public void pause()
	{
		// TODO (10/14/2014): Save game state here
		Gdx.app.debug("Lifecycle", "[GameTestState] Pause called");
		
		// Only pause on mobile, because this event fires when the window goes out of focus on 
		// desktop, instead of only when minimizing.
		if (Gdx.app.getType() != ApplicationType.Desktop)
		{
			App.setPaused(true);
		}
	}

	@Override
	public void resume()
	{
		Gdx.app.debug("Lifecycle", "[GameTestState] Resume called");
		
		if (Gdx.app.getType() != ApplicationType.Desktop)
		{
			App.setPaused(false);
		}
	}

	@Override
	public void dispose()
	{
		Gdx.app.debug("Lifecycle", "[GameTestState] Dispose called");
	}
}
