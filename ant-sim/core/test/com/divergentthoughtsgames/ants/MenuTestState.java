package com.divergentthoughtsgames.ants;

import com.badlogic.gdx.Application;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.Align;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;

/**
 * A menu state used for testing.
 * @author Christopher D. Canfield
 */
public class MenuTestState extends MenuAppState
{
	public MenuTestState()
	{
		Gdx.app.setLogLevel(Application.LOG_DEBUG);
		
		TextureAtlas atlas = new TextureAtlas(Gdx.files.internal(App.paths.Ui + "uiskin.atlas"));
		Skin skin = new Skin(Gdx.files.internal(App.paths.Ui + "uiskin.json"), atlas);

		table.row().colspan(3)
			.width(Gdx.graphics.getWidth() - 20.f)
			.height(Gdx.graphics.getHeight() - 100.f)
			.padTop(20.f);
		
		Label messageHistory = new Label("", skin);
		messageHistory.setWrap(true);
		messageHistory.setAlignment(Align.top + Align.left);
		table.add(messageHistory);
		
		messageHistory.setText("Hello\nI am a test sequence.\n\nWho are you?");
		
		table.row().colspan(3);
		TextButton startGameButton = new TextButton("Start", skin);
		table.add(startGameButton).expandX().width(100.f);
		startGameButton.addListener(new ClickListener() {
			@Override
			public void touchUp(InputEvent event, float x, float y, int pointer, int button)
			{
				App.state.setScreen(new SplashScreenTestState());
				dispose();
			}
		});
	}

	@Override
	public void dispose()
	{
	}
}
