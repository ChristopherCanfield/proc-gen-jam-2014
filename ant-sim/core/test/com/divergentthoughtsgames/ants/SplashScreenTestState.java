package com.divergentthoughtsgames.ants;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.maps.tiled.TmxMapLoader;
import com.badlogic.gdx.utils.Timer;
import com.badlogic.gdx.utils.Timer.Task;
import com.divergentthoughtsgames.ants.audio.Audio;
import com.divergentthoughtsgames.ants.graphics.Textures;

/**
 * A test state that does nothing but load a graphic and draw it to the screen.
 * @author Christopher D. Canfield
 */
public class SplashScreenTestState implements Screen
{
	private Timer timer;
	
	private boolean loadingComplete;
	
	private SpriteBatch batch;
	private Texture img;

	public SplashScreenTestState()
	{
		batch = new SpriteBatch();
		img = new Texture(App.paths.Textures + "badlogic.jpg");
		
		//////// Load game resources. ////////
		
		// Load textures.
		Textures.loadAll(App.assetManager);
		
		// Load music.
		App.assetManager.load(Audio.music.VladimirSterzerAnotherStyle, Music.class);
		App.assetManager.load(Audio.music.BurningDesertNewgrounds583089, Music.class);
		App.assetManager.load(Audio.music.Newgrounds527762, Music.class);
		App.assetManager.load(Audio.music.SynesthesiaNewgrounds542198, Music.class);
		App.assetManager.load(Audio.music.ApocalypseNewgrounds490067, Music.class);
		
		// Load ambient sounds.
		App.assetManager.load(Audio.ambient.Birds1, Music.class);
		App.assetManager.load(Audio.ambient.Birds2, Music.class);
		App.assetManager.load(Audio.ambient.Birds3, Music.class);
		App.assetManager.load(Audio.ambient.Birds4, Music.class);
		App.assetManager.load(Audio.ambient.Birds5, Music.class);
		App.assetManager.load(Audio.ambient.Birds6, Music.class);
		
		// Load maps.
		App.assetManager.setLoader(TiledMap.class, new TmxMapLoader());
		App.assetManager.load(App.paths.Maps + "test-map.tmx", TiledMap.class);
		
		App.assetManager.update();
	}

	@Override
	public void render(float delta)
	{
		Gdx.gl.glClearColor(1, 0, 0, 1);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
		
		if (App.assetManager.update() && !loadingComplete)
		{
			Gdx.app.debug("Loading", "Resource loading complete.");
			
			loadingComplete = true;
			timer = new Timer();
			timer.scheduleTask(new Task() {
				@Override
				public void run()
				{
					App.state.setScreen(new GameTestState());
				}
			}, 0);
		}
		
		if (!loadingComplete)
		{
			float progress = App.assetManager.getProgress();
			Gdx.app.debug("Loading", "Loading progress: " + progress);
			// TODO (10/14/2014): Show progress bar here.
		}
		
		batch.begin();
		batch.draw(img, 0, 0);
		batch.end();
	}

	@Override
	public void resize(int width, int height)
	{
		Gdx.app.debug("Lifecycle", "[SplashScreen] Resize called");
	}

	@Override
	public void show()
	{
		Gdx.app.debug("Lifecycle", "[SplashScreen] Show called");
	}

	@Override
	public void hide()
	{
		Gdx.app.debug("Lifecycle", "[SplashScreen] Hide called");
	}

	@Override
	public void pause()
	{
		Gdx.app.debug("Lifecycle", "[SplashScreen] Pause called");
	}

	@Override
	public void resume()
	{
		Gdx.app.debug("Lifecycle", "[SplashScreen] Resume called");
	}

	@Override
	public void dispose()
	{
	}
}
