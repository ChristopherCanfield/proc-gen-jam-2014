/*
 * Christopher D. Canfield
 * Divergent Thoughts Games
 *           2014
 */
package com.divergentthoughtsgames.ants.nav;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class NodeTest
{

	@Before
	public void setUp() throws Exception
	{
	}

	@Test
	public void hashCode_Equals()
	{
		Node node1 = new Node(14, 20, false);
		Node node2 = new Node(14, 20, false);
		Node node3 = new Node(16, 3, false);
		Node node4 = new Node(20, 14, false);
		
		assertEquals(node1.hashCode(), node1.hashCode());
		assertEquals(node1.hashCode(), node2.hashCode());
		assertNotEquals(node1, node3);
		assertNotEquals(node1, node4);
		
		assertEquals(node1, node1);
		assertEquals(node1, node2);
		assertNotEquals(node1, node3);
		assertNotEquals(node1, node4);
	}

	@Test
	public void testAddEdgeGetEdges()
	{
		Edge edge = new Edge(1);
		Node node = new Node(16, 16, false);
		node.addEdge(edge);
		
		assertEquals(node.getEdges().size(), 1);
		assertSame(edge, node.getEdges().get(0));
	}

	@Test
	public void testGetXGetY()
	{
		Node node = new Node(5, 14, false);
		assertEquals(5, node.getX());
		assertEquals(14, node.getY());
	}

	@Test
	public void testGetRowIndexGetColumnIndex()
	{
		Node node = new Node(16, 64, false);
		assertEquals(16 / Node.Size, node.getColumnIndex());
		assertEquals(64 / Node.Size, node.getRowIndex());
		
		Node node2 = new Node(5, 8, false);
		assertEquals(5 / Node.Size, node2.getColumnIndex());
		assertEquals(8 / Node.Size, node2.getRowIndex());
		
		Node node3 = new Node(65, 129, false);
		assertEquals(65 / Node.Size, node3.getColumnIndex());
		assertEquals(129 / Node.Size, node3.getRowIndex());
	}
}
