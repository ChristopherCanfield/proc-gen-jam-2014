package com.divergentthoughtsgames.ants.desktop;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import com.divergentthoughtsgames.ants.ExtendedGame;

public class DesktopLauncher {
	@SuppressWarnings("unused")
	public static void main (String[] arg) {
		LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();
		config.title = "Ant Simulator";
		config.width = 800;
		config.height = 600;
		
		new LwjglApplication(new ExtendedGame(), config);
	}
}
