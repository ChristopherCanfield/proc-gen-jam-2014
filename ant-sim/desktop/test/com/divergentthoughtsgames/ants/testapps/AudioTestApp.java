package com.divergentthoughtsgames.ants.testapps;

import java.util.ArrayList;

import com.badlogic.gdx.Application;
import com.badlogic.gdx.ApplicationListener;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import com.badlogic.gdx.utils.Timer.Task;
import com.divergentthoughtsgames.ants.App;
import com.divergentthoughtsgames.ants.audio.Audio;

public class AudioTestApp implements ApplicationListener  
{
	@SuppressWarnings("unused")
	public static void main (String[] arg) {
		LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();
		config.title = "Audio Test";
		config.width = 800;
		config.height = 600;
		
		new LwjglApplication(new AudioTestApp(), config);
	}
	
	private boolean soundsAlreadyLoaded = false;
	
	@Override
	public void create()
	{
		App.assetManager.load(Audio.music.VladimirSterzerAnotherStyle, Music.class);
		
		// Load ambient sounds.
		App.assetManager.load(Audio.ambient.Birds1, Music.class);
		App.assetManager.load(Audio.ambient.Birds2, Music.class);
		App.assetManager.load(Audio.ambient.Birds3, Music.class);
		App.assetManager.load(Audio.ambient.Birds4, Music.class);
		App.assetManager.load(Audio.ambient.Birds5, Music.class);
		App.assetManager.load(Audio.ambient.Birds6, Music.class);
		
		// Load test sound effects.
		App.assetManager.load(App.paths.Sfx + "test/mine_explosion.wav", Sound.class);
		App.assetManager.load(App.paths.Sfx + "test/pillbox_hit.wav", Sound.class);
		App.assetManager.load(App.paths.Sfx + "test/tank_explosion.wav", Sound.class);
		App.assetManager.load(App.paths.Sfx + "test/tank_hit.wav", Sound.class);
		App.assetManager.load(App.paths.Sfx + "test/wall_hit.wav", Sound.class);
		
		Gdx.app.setLogLevel(Application.LOG_DEBUG);
	}

	@Override
	public void resize(int width, int height)
	{
	}

	@Override
	public void render()
	{
		if (App.assetManager.update())
		{
			if (!soundsAlreadyLoaded)
			{
				soundsAlreadyLoaded = true;
				
				// TODO (10/21/2014): play all sounds and music.
				ArrayList<String> playlist = new ArrayList<String>();
				playlist.add(Audio.music.VladimirSterzerAnotherStyle);
				App.audio.startMusic(playlist);
				
				ArrayList<String> ambientPlaylist = new ArrayList<String>();
				ambientPlaylist.add(Audio.ambient.Birds1);
				ambientPlaylist.add(Audio.ambient.Birds2);
				ambientPlaylist.add(Audio.ambient.Birds3);
				ambientPlaylist.add(Audio.ambient.Birds4);
				ambientPlaylist.add(Audio.ambient.Birds5);
				ambientPlaylist.add(Audio.ambient.Birds6);
				App.audio.startAmbientSounds(ambientPlaylist);
				
				App.timer.scheduleTask(new Task() {
					@Override
					public void run()
					{
						App.exit();
					}
				}, 10);
				
				App.audio.play(App.paths.Sfx + "test/mine_explosion.wav");
				
				App.timer.scheduleTask(new Task() {
					@Override public void run() {
						App.audio.play(App.paths.Sfx + "test/pillbox_hit.wav");
						Gdx.app.debug("AudioTestApp", "pillbox_hit.wav played");
					}
				}, 2);
				
				App.timer.scheduleTask(new Task() {
					@Override public void run() {
						App.audio.play(App.paths.Sfx + "test/tank_explosion.wav");
						Gdx.app.debug("AudioTestApp", "tank_explosion.wav played");
					}
				}, 4);
				
				App.timer.scheduleTask(new Task() {
					@Override public void run() {
						App.audio.play(App.paths.Sfx + "test/tank_hit.wav");
						Gdx.app.debug("AudioTestApp", "tank_hit.wav played");
					}
				}, 6);
				
				App.timer.scheduleTask(new Task() {
					@Override public void run() {
						App.audio.play(App.paths.Sfx + "test/wall_hit.wav");
						Gdx.app.debug("AudioTestApp", "wall_hit.wav played");
					}
				}, 8);
			}
		}
	}

	@Override
	public void pause()
	{
	}

	@Override
	public void resume()
	{
	}

	@Override
	public void dispose()
	{
	}
}
