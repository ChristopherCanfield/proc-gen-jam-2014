package com.divergentthoughtsgames.ants.testapps;

import com.badlogic.gdx.Application;
import com.badlogic.gdx.ApplicationListener;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import com.badlogic.gdx.graphics.GL20;
import com.divergentthoughtsgames.ants.App;
import com.divergentthoughtsgames.ants.Debug;
import com.divergentthoughtsgames.ants.graphics.Textures;
import com.divergentthoughtsgames.ants.nav.Node;
import com.divergentthoughtsgames.ants.world.Ant;
import com.divergentthoughtsgames.ants.world.AntColony;
import com.divergentthoughtsgames.ants.world.World;

public class GraphicsTestApp implements ApplicationListener  
{
	@SuppressWarnings("unused")
	public static void main (String[] arg) {
		LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();
		config.title = "Graphics Test";
		config.width = 800;
		config.height = 600;
		
		new LwjglApplication(new GraphicsTestApp(), config);
	}
	
	private boolean spritesAdded = false;
	
	private Node node1;
	private Node node2;
	
	@Override
	public void create()
	{
		App.initialize();
		App.world = new World(10, 10);
		App.debugSettings = (Debug.DrawNavNodes + Debug.DrawNavPaths);
		
		Textures.loadAll(App.assetManager);
		
		Gdx.app.setLogLevel(Application.LOG_DEBUG);
	}

	@Override
	public void resize(int width, int height)
	{
	}

	@Override
	public void render()
	{
		Gdx.gl.glClearColor(0.25f, 0.5f, 0.75f, 1f);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
		
		if (App.assetManager.update())
		{
			if (!spritesAdded)
			{
				spritesAdded = true;
				
				AntColony colony = App.world.addColony("Test");
				Ant ant = App.world.addAnt(colony.getId());
				ant.setPosition(10, 10);
				
				node1 = new Node(0, 0);
				node2 = new Node(64, 64);
				App.graphics.addSprite(node1);
				App.graphics.addSprite(node1, node2);
			}
			
			App.graphics.render();
		}
	}

	@Override
	public void pause()
	{
	}

	@Override
	public void resume()
	{
	}

	@Override
	public void dispose()
	{
	}
}
